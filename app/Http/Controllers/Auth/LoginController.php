<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use DB;
use Session;
use Waavi\Sanitizer\Sanitizer;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/landholding/beneficiaries';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'name'; //or whatever field
    }

    //Function to Redirect user to IKM LSGD CentralAuthService
    public function ext_login(){
        //ext_link
        $url = "https://cas.lsgkerala.gov.in/openid/loginopenidhash.htm";
        //rand_unique_length
        $length = 15;
        //cust_id
        $cuid = join('', array_map(function($value) { return $value == 1 ? mt_rand(1, 9) : mt_rand(0, 9); }, range(1, $length)));;
        Session::put('cuid',$cuid);
        $caid = 202;
        $key = "lifemission@ikm#1999\$openid";
        $today = date("d-m-Y");
        $ca = $cuid.$caid.$today.$key;
        $cahash = hash('sha256',$ca);
        $url .= "?cuid=".$cuid."&caid=".$caid."&cahash=".$cahash;
        return redirect($url);
    }

    //Function to handle Redirect user from IKM LSGD CentralAuthService
    public function verify_ext_login(Request $request){
        if($request->userdet == null){
            abort(404);
        }
        $user_det_response = $request->userdet;
        $user_det_array = explode("~",$user_det_response);
//        return $user_det_array;

//        return $user_details;
        $user_details = [
            'unique_id' => $user_det_array[0],
            'local_body_id' => $user_det_array[1],
            'username' => $user_det_array[2],
            'cuid' => $user_det_array[3],
            'fullname' => $user_det_array[4],
            'unknown1' => $user_det_array[5],
            'designation' => $user_det_array[6],
            'mobileno' => $user_det_array[7],
            'unknown2' => $user_det_array[8],
            'response_hash' => $user_det_array[9],
        ];
        $filters = [
        'unique_id' =>  'trim|escape',
        'local_body_id'     =>  'trim|escape',
        'username'     =>  'trim|escape',
        'cuid'     =>  'trim|escape',
        'fullname'     =>  'trim|escape',
        'unknown1'     =>  'trim|escape',
        'designation'     =>  'trim|escape',
        'mobileno'     =>  'trim|escape',
        'unknown2'     =>  'trim|escape',
        'response_hash'     =>  'trim|escape',
        ];
        $sanitizer  = new Sanitizer($user_details, $filters);
        $user_details = $sanitizer->sanitize();
//        return $user_details;
        //validate response hash
        $cuid = Session::get('cuid');
        if($cuid != $user_details['cuid']){
            abort(404);
        }
        $user_id = DB::table('user_localbody_mapping')
        ->where('local_body_id',$user_details['local_body_id'])->value('user_id');
        $user = User::find($user_id);
        if($user == null){
            abort(404);
        }
	//
	Log::info('LoginController: Local Body id: '.$user_details['local_body_id'].'! IP address: '.$request->ip().'! Timestamp: '.date("Y-m-d H:i:s"));
        // Set Auth Details
        \Auth::login($user);

        // Redirect home page
        return redirect('/landholding/beneficiaries');
    }
}
