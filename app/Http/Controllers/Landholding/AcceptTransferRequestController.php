<?php

namespace App\Http\Controllers\Landholding;
use App\Models\Landholding\LocalBodyTransfer;
use App\Models\Landholding\Beneficiary;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
class AcceptTransferRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid=Auth::user()->id;
        $signature_status = DB::table('lsg_aggregation')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id','=', 'lsg_aggregation.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->value('signature_status');


        if($request->ajax()){
            $lb_id=Session::get('lb_id');
            $data = LocalBodyTransfer::where('to_localbody',$lb_id)
            ->join('landholding_beneficiaries','landholding_beneficiaries.beneficiary_id','local_body_transfer.beneficiary_id')
            ->join('local_body','local_body.local_body_id','local_body_transfer.from_localbody')
            ->where('status_flag',0)
            // ->select([
            //     'local_body_transfer.beneficiary_id',
            //     'landholding_beneficiaries.beneficiary_name',
            //     'local_body.local_body_display_name',
            //     'local_body_transfer.transfer_id',
            //     'local_body_transfer.status_flag',
            //     'local_body_transfer.from_localbody',
            //     'local_body_transfer.to_localbody',
            //     'local_body_transfer.created_at',
            // ])
            ->get();
            return Datatables::of($data)->make(true);
            }
        return view("Landholding/accepttransferrequest",compact('signature_status'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lb_id=Session::get('lb_id');
        $update_flag = LocalBodyTransfer::where('beneficiary_id',$id)->update(['status_flag'=>1]);
        $update_ben_flag = Beneficiary::where('beneficiary_id',$id)->update(['local_body_id'=>$lb_id]);
        if($update_flag)
          return json_encode(['status'=>'success','message'=>'request updated successfully!']);
        else
          return json_encode(['status'=>'warning','message'=>'failed !']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
