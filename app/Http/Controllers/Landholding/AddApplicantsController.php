<?php

namespace App\Http\Controllers\Landholding;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Landholding\LandholdingWorkflow;
use App\Models\Landholding\NewApplicants;
use DB;
use DataTables;
use Auth;
use Waavi\Sanitizer\Sanitizer;
use App\Models\Landholding\Beneficiary;
use DateTime;
use Carbon\Carbon;
use Session;
class AddApplicantsController extends Controller
{
    private $dateFlag;
    private $endDate;
    private $today;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        setlocale(LC_TIME, 'ml_IN.UTF8');
        //Carbon :: setlocale('mal');
        //echo Carbon::today()->format('d-M-Y');
        $timezone = date_default_timezone_get();
        $this->today = date('d-M-Y');
        $this->endDate = date('30-09-2018');
        $this->dateFlag=0;
        if(strtotime($this->today) > strtotime($this->endDate))
        {
            $this->dateFlag=1;
        }
    }

    public function index(Request $request, $cat_id)
    {
        if($cat_id > 4 OR $cat_id < 1){
            abort(403);
        }
        $userid=Auth::user()->id; //neew to use session variables
        $dateFlag = $this->dateFlag;
        $lastdate = strftime("%e %B %Y", strtotime($this->endDate));
        $lbdetails = DB::table('local_body')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id', 'local_body.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->select('local_body_display_name','local_body.local_body_id')->get();

        $signature_status = DB::table('lsg_aggregation')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id','=', 'lsg_aggregation.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->value('signature_status');

        if($request->ajax()){
            $data = DB::table('new_applicants')
            ->join('beneficiary_category','beneficiary_category.category_id','new_applicants.category_id')
            ->leftJoin('present_placeof_stay','present_placeof_stay.place_of_stay_id','new_applicants.placeof_stay_id')
           ->where('local_body_id',$lbdetails[0]->local_body_id)
           ->where('new_applicants.applicant_category_id', $cat_id) //category id passed from blade
           ->get();
           return Datatables::of($data)->make(true);
           }
        return view('/Landholding/applicants'.$cat_id,compact('signature_status','dateFlag','lastdate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($this->dateFlag == 1){
            return json_encode(['status'=>'warning','message'=>'Unchanged data!']);
        }
        $userid=Auth::user()->id;

        $lbid = DB::table('local_body')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id','=', 'local_body.local_body_id')
        ->select('user_localbody_mapping.local_body_id')->where('user_localbody_mapping.user_id',$userid)
        ->get();

        $form_data = [
            'local_body_id'  => $lbid[0]->local_body_id,
            'applicant_name'=>$request->name,
            'addr_house_num'=>$request->addr_house_num,
            'addr_house_name'=>$request->addr_house_name,
            'addr_house_location'=>$request->addr_house_location,
            'pincode'=>$request->addr_house_pincode,
            'aadhaar'=>$request->adhaar,
            'ration_number'=>$request->ration,
            'gender'=>$request->gender,
            'category_id'=>$request->category,
            'applicant_category_id'=>$request->applicant_category_id,
            'placeof_stay_id'=>$request->placeofstay,
            'authentication_date'=>$request->authenticationdate,
            'reason_for_deferring'=>$request->reason,
            'landless_ben_sl_num'=>$request->landless_ben_id,
            'minority'=> ($request->minority=='on' ? 1: 0),
            'handicapped'=> ($request->handicapped=='on' ? 1: 0),
            'women_lead'=> ($request->womenlead=='on' ? 1: 0),
            'fisherman'=> ($request->fisherman=='on' ? 1: 0),
            'plantation'=> ($request->plantation=='on' ? 1: 0),
            'st_remote'=> ($request->st_remote=='on' ? 1: 0),
            'resolution_num'=>$request->resolutionnum,
            'resolution_date'=>$request->resolutiondate,
            'updated_at' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),

        ];
        if($form_data['applicant_category_id'] == 4){
            $form_data['placeof_stay_id']=NULL;
            $form_data['house_type'] = $request->house_type;
            $form_data['damage_type'] = $request->damage_type;
        }

        $filters = [
            'applicant_name' =>  'trim|escape',
            'addr_house_num' =>  'trim|escape',
            'addr_house_name' =>  'trim|escape',
            'addr_house_location' =>  'trim|escape',
            'pincode' =>  'trim|escape',
            'ration_number' =>  'trim|escape',
            'aadhaar'     =>  'trim|escape',
            'gender'     =>  'trim|escape',
            'category_id'     =>  'trim|escape',
            'minority'     =>  'trim|escape',
            'handicapped'     =>  'trim|escape',
            'women_lead'     =>  'trim|escape',
            'fisherman'     =>  'trim|escape',
            'plantation'     =>  'trim|escape',
            'st_remote'     =>  'trim|escape',
            'applicant_category_id'     =>  'trim|escape',
            'authentication_date'     =>  'trim|escape',
            'reason_for_deferring'     =>  'trim|escape',
            'placeof_stay_id'     =>  'trim|escape',
            'house_type'     =>  'trim|escape',
            'damage_type'     =>  'trim|escape',
            'resolution_date'     =>  'trim|escape',
            'resolution_num'     =>  'trim|escape',

        ];
        $sanitizer  = new Sanitizer($form_data, $filters);
        $form_data = $sanitizer->sanitize();

        $insertflag = 0;

        if(($form_data['gender'] != 'F' and $form_data['women_lead'] == 1) or ($form_data['category_id'] != 2 and $form_data['st_remote'] == 1)){
            return json_encode(['status'=>'danger','message'=>'Inconsistency in data!']);
        }

        if (empty($form_data['authentication_date']))
        {
            $form_data['authentication_date']=NULL;
        }
        if (!empty($form_data['placeof_stay_id']))
        {
            $insertflag=NewApplicants::insert($form_data);
        }else
        {
            $form_data['placeof_stay_id']=NULL;
            $insertflag=NewApplicants::insert($form_data);
        }


              // $insertflag=NewApplicants::insert($form_data);
        if($insertflag)
        return json_encode(['status'=>'success','message'=>'inserted successfully!']);
      else
        return json_encode(['status'=>'warning','message'=>'Unchanged data!']);


        //return view('Landholding/applicants');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$cat_id, $id)
    {
        $form_data = [
            'aadhaar'=>$request->adhaar,
            'ration_number'=>$request->ration,
            'gender'=>$request->gender,
            'category_id'=>$request->category,
            'applicant_category_id'=>$request->applicant_category_id,
            'placeof_stay_id'=>$request->placeofstay,
            'authentication_date'=>$request->authenticationdate,
            'reason_for_deferring'=>$request->reason,
            'landless_ben_sl_num'=>$request->landless_ben_id,
            'minority'=> ($request->minority=='on' ? 1: 0),
            'handicapped'=> ($request->handicapped=='on' ? 1: 0),
            'women_lead'=> ($request->womenlead=='on' ? 1: 0),
            'fisherman'=> ($request->fisherman=='on' ? 1: 0),
            'plantation'=> ($request->plantation=='on' ? 1: 0),
            'st_remote'=> ($request->st_remote=='on' ? 1: 0),
            'resolution_num'=>$request->resolutionnum,
            'resolution_date'=>$request->resolutiondate,
            'updated_at' => date("Y-m-d H:i:s"),

        ];
        if($form_data['applicant_category_id'] == 4){
            $form_data['placeof_stay_id']=NULL;
            $form_data['house_type'] = $request->house_type;
            $form_data['damage_type'] = $request->damage_type;
        }
        $filters = [
            'ration_number' =>  'trim|escape',
            'aadhaar'     =>  'trim|escape',
            'gender'     =>  'trim|escape',
            'category_id'     =>  'trim|escape',
            'minority'     =>  'trim|escape',
            'handicapped'     =>  'trim|escape',
            'women_lead'     =>  'trim|escape',
            'fisherman'     =>  'trim|escape',
            'plantation'     =>  'trim|escape',
            'st_remote'     =>  'trim|escape',
            'landless_ben_sl_num'     =>  'trim|escape',
            'applicant_category_id'     =>  'trim|escape',
            'authentication_date'     =>  'trim|escape',
            'reason_for_deferring'     =>  'trim|escape',
            'placeof_stay_id'     =>  'trim|escape',
            'resolution_date'     =>  'trim|escape',
            'resolution_num'     =>  'trim|escape',
        ];
        $sanitizer  = new Sanitizer($form_data, $filters);
        $form_data = $sanitizer->sanitize();
       // return $form_data;
       if($this->dateFlag == 1){
        return json_encode(['status'=>'warning','message'=>'Unchanged data!']);
    }
       $insertflag = 0;

       if(($form_data['gender'] != 'F' and $form_data['women_lead'] == 1) or ($form_data['category_id'] != 2 and $form_data['st_remote'] == 1)){
           return json_encode(['status'=>'danger','message'=>'Inconsistency in data!']);
       }

       if (empty($form_data['authentication_date'])  )
       {
           $form_data['authentication_date']=NULL;
       }
       if (!empty($form_data['placeof_stay_id']))
       {
        $insertflag=NewApplicants::where('applicant_id',$id)->update($form_data);
       }else
       {
           $form_data['placeof_stay_id']=NULL;
           $insertflag=NewApplicants::where('applicant_id',$id)->update($form_data);
       }
       // return $form_data;
        if($insertflag)
        return json_encode(['status'=>'success','message'=>'updated successfully!']);
      else
        return json_encode(['status'=>'warning','message'=>'Unchanged data!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cat_id,$id)
    {
        $res=NewApplicants::where('applicant_id',$id)->delete();
        if($res)
        return json_encode(['status'=>'success','message'=>'Record Deleted Successfully!']);
      else
        return json_encode(['status'=>'warning','message'=>'Record Delete Failed!']);
    }

    public function submitapplicants(Request $request, $cat_id)
    {
      $lb_id = Session::get('lb_id');
      // echo $lb_id;
      $applicants = NewApplicants::where('local_body_id',$lb_id)
      ->where('applicant_category_id',$cat_id)
      ->whereNull('beneficiary_id')
      ->get()->toArray();
      if(count($applicants) == 0){
        return json_encode(['status'=>'warning','message'=>'All applicants have been copied already!']);
      }
      foreach($applicants as $applicant){
        $applicant['beneficiary_name'] = $applicant['applicant_name'];
        $applicant['workflow_stage_id'] = 2;
        $applicant['reg_id'] = abs(Beneficiary::max('reg_id'))+1; //hard coded for new applicants

        $applicant_id = $applicant['applicant_id'];
        //removing unnecessary fields to copy
        unset($applicant['applicant_id']);
        unset($applicant['applicant_name']);
        unset($applicant['applicant_category_id']);
        unset($applicant['placeof_stay_id']);
        unset($applicant['authentication_date']);
        unset($applicant['reason_for_deferring']);
        unset($applicant['landless_ben_sl_num']);
        unset($applicant['house_type']);
        unset($applicant['damage_type']);
        unset($applicant['created_at']);
        unset($applicant['updated_at']);
        unset($applicant['deleted_at']);
        unset($applicant['beneficiary_id']);

        // print_r($applicant);
        $update_flag=$workflow_flag=null;
        DB::transaction(function() use ($applicant_id, $applicant, $update_flag, $workflow_flag){
          $beneficiary = Beneficiary::create($applicant);
          $beneficiary_id = $beneficiary->beneficiary_id;
          $workflow_data = [
            'workflow_stage_id' => 2, //stage 0
            'beneficiary_id' => $beneficiary_id,
            'updated_at' => date("Y-m-d H:i:s"),
          ];
          $workflow_flag = LandholdingWorkflow::insert($workflow_data);
          $update_flag = NewApplicants::where('applicant_id',$applicant_id)->update(['beneficiary_id' => $beneficiary_id]);
        });
      }
      $applicants_pending = NewApplicants::where('local_body_id',$lb_id)
      ->where('applicant_category_id',$cat_id)
      ->whereNull('beneficiary_id')
      ->count();
      if($applicants_pending==0){
        return json_encode(['status'=>'success','message'=>'All applicants have been copied!']);
      } else {
        return json_encode(['status'=>'danger','message'=>'There is some error in copying!']);
      }
    }
}
