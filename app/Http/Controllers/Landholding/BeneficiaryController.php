<?php
namespace App\Http\Controllers\Landholding;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Landholding\Beneficiary;
use App\Models\Landholding\LandholdingWorkflow;
use DB;
use DataTables;
use Auth;
use Session;
use Illuminate\Support\Facades\Log;
use Waavi\Sanitizer\Sanitizer;

class BeneficiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $userid=Auth::user()->id;
        // $signaturestatus = DB::table('lsg_aggregation')
        // ->join('user_localbody_mapping','user_localbody_mapping.local_body_id','=', 'lsg_aggregation.local_body_id')
        // ->select('signature_status')->where('user_localbody_mapping.user_id',$userid)
        // ->get();
        $signature_status = DB::table('lsg_aggregation')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id','=', 'lsg_aggregation.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->value('signature_status');

        $lbdetails = DB::table('local_body')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id', 'local_body.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->select('local_body_display_name','local_body.local_body_id')->get();

        Session::put('user_id',$userid);
        Session::put('lb_id',$lbdetails[0]->local_body_id);
        Session::put('lb_name',$lbdetails[0]->local_body_display_name);


//	DB::enableQueryLog();
        if($request->ajax()){
//	    $query = DB::getQueryLog();
//          $lastQuery = end($query);
//	    print_r($lastQuery);
//	    Log::info(': Error! query:'.vsprintf(str_replace('?', '`%s`', $lastQuery['query']), $lastQuery['bindings']));
            // return Datatables::of(Beneficiary::query())->make(true);
        $data = DB::table('landholding_beneficiaries')
        ->join('beneficiary_category','beneficiary_category.category_id','landholding_beneficiaries.category_id')
        ->where('local_body_id',$lbdetails[0]->local_body_id)
        ->get();
        return Datatables::of($data)->make(true);
        }

        //return view('Landholding/beneficiaries')->with('signature',$signaturestatus);
        return view('Landholding/beneficiaries',compact('signature_status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
	    $form_data = [
            'ration_number'=>$request->ration,
            'aadhaar'=>$request->adhaar,
            'gender'=>$request->gender,
            'category_id'=>$request->category,
            'resolution_date'=>$request->resolutiondate,
            'resolution_num'=>$request->resolutionnum,
            'possession_verified'=> ($request->possession=='on' ? 1: 0),
            'minority'=> ($request->minority=='on' ? 1: 0),
            'handicapped'=> ($request->handicapped=='on' ? 1: 0),
            'women_lead'=> ($request->womenlead=='on' ? 1: 0),
            'fisherman'=> ($request->fisherman=='on' ? 1: 0),
            'plantation'=> ($request->plantation=='on' ? 1: 0),
            'st_remote'=> ($request->st_remote=='on' ? 1: 0),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ];

        $filters = [
            'ration_number' =>  'trim|escape',
            'aadhaar'     =>  'trim|escape',
            'gender'     =>  'trim|escape',
            'category_id'     =>  'trim|escape',
            'resolution_date'     =>  'trim|escape',
            'resolution_num'     =>  'trim|escape',
            'possession_verified'     =>  'trim|escape',
            'minority'     =>  'trim|escape',
            'handicapped'     =>  'trim|escape',
            'women_lead'     =>  'trim|escape',
            'fisherman'     =>  'trim|escape',
            'plantation'     =>  'trim|escape',
            'st_remote'     =>  'trim|escape',
        ];
        $sanitizer  = new Sanitizer($form_data, $filters);
        $form_data = $sanitizer->sanitize();

        $update_flag=$workflow_flag=null;

        $data_validity = 1;
        //cross validating (gender male and woman lead family) or (ST category and st_remote flag)
        if(($form_data['gender'] != 'F' and $form_data['women_lead'] == 1) or ($form_data['category_id'] != 2 and $form_data['st_remote'] == 1)){
            return json_encode(['status'=>'danger','message'=>'Inconsistency in data!']);
        }
        //cross validating resolution date and resolution number
        if (empty($form_data['resolution_date']) or empty($form_data['resolution_num'])) {
            $form_data['resolution_num'] = null;
            $form_data['resolution_date'] = null;
            $form_data['possession_verified'] = 0;
            $update_flag = Beneficiary::where('beneficiary_id',$id)->update($form_data);
        }
        if (!empty($form_data['resolution_date']) and !empty($form_data['resolution_num'])) {
            if ($form_data['possession_verified'] == 0) {
                $form_data['workflow_stage_id'] = 2;  //stage 0

                $workflow_data = [
                    'workflow_stage_id' => 2, //stage 0
                    'beneficiary_id' => $id,
                    'updated_at' => date("Y-m-d H:i:s"),
                ];

                $record_count = LandholdingWorkflow::where('beneficiary_id','=',$id)->count();
                if ($record_count == 0) {
                    $workflow_flag = LandholdingWorkflow::insert($workflow_data);
                    $update_flag = Beneficiary::where('beneficiary_id',$id)->update($form_data);
                } else if ($record_count == 1) {
                    $workflow_flag = LandholdingWorkflow::where('beneficiary_id',$id)->update($workflow_data);
                    $update_flag = Beneficiary::where('beneficiary_id',$id)->update($form_data);
                } else {
	                $update_flag = Log::error('BeneficiaryController: Beneficiary id '.$id.'. Inconsistent workflow detected.');
                }
            }
            else {
                $form_data['workflow_stage_id'] = 3;  //stage 1

                $workflow_data = [
                    'workflow_stage_id' => 3, //stage 1
                    'beneficiary_id' => $id,
                    'updated_at' => date("Y-m-d H:i:s"),
                ];

                $record_count = LandholdingWorkflow::where('beneficiary_id','=',$id)->count();
                if ($record_count == 0) {
                    $workflow_flag = LandholdingWorkflow::insert($workflow_data);
                    $update_flag = Beneficiary::where('beneficiary_id',$id)->update($form_data);
                } else if ($record_count == 1) {
                    $workflow_flag = LandholdingWorkflow::where('beneficiary_id',$id)->update($workflow_data);
                    $update_flag = Beneficiary::where('beneficiary_id',$id)->update($form_data);
                } else {
	            $update_flag = Log::error('BeneficiaryController: Beneficiary id '.$id.'. Inconsistent workflow detected.');
                }
            }
        }
        if($update_flag OR $workflow_flag)
          return json_encode(['status'=>'success','message'=>'Updated successfully!']);
        else
          return json_encode(['status'=>'warning','message'=>'Unchanged data!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
