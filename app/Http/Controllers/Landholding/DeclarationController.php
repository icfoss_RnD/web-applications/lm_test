<?php
namespace App\Http\Controllers\Landholding;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DataTables;
use App\Models\Landholding\Beneficiary;
use App\Models\Landholding\LSGAggregation;
use Waavi\Sanitizer\Sanitizer;
use Storage;

class DeclarationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid=Auth::user()->id;

        $lbdetails = DB::table('local_body')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id', 'local_body.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->select('local_body_display_name','local_body.local_body_id')->get();

        $signature_status = DB::table('lsg_aggregation')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id','=', 'lsg_aggregation.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->value('signature_status');
        $beneficiarycount=DB::table('landholding_beneficiaries')
        ->where('local_body_id',$lbdetails[0]->local_body_id)->count();

        if($request->ajax()){
            $data = DB::table('landholding_beneficiaries')
            ->join('beneficiary_category','beneficiary_category.category_id','landholding_beneficiaries.category_id')
            ->whereNotNull('resolution_date')
            ->whereNotNull('resolution_num')
            ->where('local_body_id',$lbdetails[0]->local_body_id)
            ->get();
            // $data = Beneficiary::query()
            // ->join('beneficiary_category','beneficiary_category.category_id','landholding_beneficiaries.category_id')
            // ->whereNotNull('resolution_date')
            // ->whereNotNull('resolution_num')
            // ->where('local_body_id',$lbdetails[0]->local_body_id);
            return Datatables::of($data)->make(true);
        }
        return view('Landholding/declarations',compact('lbdetails','signature_status','beneficiarycount','validatedbeneficiaries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid=Auth::user()->id;

        $lbdetails = DB::table('local_body')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id', 'local_body.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->select('local_body_display_name','local_body.local_body_id')->get();

        //deleting previous records of declaration to avoid duplication against localbody
        $del = DB::table('lsg_aggregation')
        ->where('local_body_id',$lbdetails[0]->local_body_id)
        ->delete();

        $form_data = [
            'local_body_id'=> $lbdetails[0]->local_body_id,
            'beneficiary_count'=> $request->count,
            'comments'=> $request->comments,
            'signature_status'=> ($request->sign=='on' ? 1: 0),
            'signed_data' => $request->signedData,
            'financial_year' => 2018,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ];

        $filters = [
            'beneficiary_count' =>  'trim|escape',
            'comments'     =>  'trim|escape',
            'signature_status'     =>  'trim|escape',
            'signed_data'     =>  'trim|escape',
        ];
        $sanitizer  = new Sanitizer($form_data, $filters);
        $form_data = $sanitizer->sanitize();

        if($form_data['signed_data']!=null and $form_data['beneficiary_count']!=null and $form_data['signature_status'] == 1){
            //getting list of beneficiary IDS data to be signed
            $beneficiary_ids = Beneficiary::where('local_body_id',$lbdetails[0]->local_body_id)
            ->whereNotNull('resolution_date')
            ->whereNotNull('resolution_num')
            ->get(['beneficiary_id'])
            ->toJson();
            $folder = "signed_beneficiaries";
            //create folder if not exists
            Storage::makeDirectory($folder);
            $filename_to_write_records = "LocalBody: ".$lbdetails[0]->local_body_id." - ".date("Y-m-d H:i:s").".json";
            //writing to file
            Storage::put($folder."/".$filename_to_write_records,$beneficiary_ids);
            //signing
            $flag=DB::table('lsg_aggregation')->insert($form_data);
        }
        return redirect()->action('Landholding\DeclarationController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
