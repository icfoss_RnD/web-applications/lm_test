<?php

namespace App\Http\Controllers\Landholding;

use App\Models\Landholding\LSGFundAggregation;
use App\Models\Landholding\Beneficiary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Support\Facades\Log;
use Waavi\Sanitizer\Sanitizer;
use DataTables;

class FundDeclarationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $userid=Auth::user()->id;

        $lbdetails = DB::table('local_body')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id', 'local_body.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->select('local_body_display_name','local_body.local_body_id')->get();
        //	DB::enableQueryLog();

        $countdata = DB::select(DB::raw('select landholding_workflow.workflow_stage_id,st_remote,count(landholding_beneficiaries.beneficiary_id) as total
        from landholding_workflow 
        join landholding_beneficiaries 
        on landholding_workflow.beneficiary_id=landholding_beneficiaries.beneficiary_id 
        left join workflow_demand_mapping 
        on workflow_demand_mapping.workflow_id=landholding_workflow.workflow_id 
        where workflow_demand_mapping.demand_id is NULL 
        and installment_release_status = 0
        and local_body_id = '.$lbdetails[0]->local_body_id.'
        group by landholding_workflow.workflow_stage_id,st_remote;'));

        $funddata = array(
            'stage1' => array(0,0), // st_remote_pending,others_pending
            'stage2' => array(0,0),
            'stage3' => array(0,0),
            'stage4' => array(0,0),
            'stage5' => array(0,0),
        );
        foreach($countdata as $index=>$value){
            switch($value->workflow_stage_id){
                case 3: if($value->st_remote==1){
                        $funddata['stage1'][0] = $value->total;
                        }
                        elseif($value->st_remote==0){
                            $funddata['stage1'][1] = $value->total;
                        }
                        break;
                case 4: if($value->st_remote==1){
                        $funddata['stage2'][0] = $value->total;
                        }
                        elseif($value->st_remote==0){
                            $funddata['stage2'][1] = $value->total;
                        }
                        break;
                case 5: if($value->st_remote==1){
                        $funddata['stage3'][0] = $value->total;
                        }
                        elseif($value->st_remote==0){
                            $funddata['stage3'][1] = $value->total;
                        }
                        break;
                case 6: if($value->st_remote==1){
                        $funddata['stage4'][0] = $value->total;
                        }
                        elseif($value->st_remote==0){
                            $funddata['stage4'][1] = $value->total;
                        }
                        break;
                case 7: if($value->st_remote==1){
                        $funddata['stage5'][0] = $value->total;
                        }
                        elseif($value->st_remote==0){
                            $funddata['stage5'][1] = $value->total;
                        }
                        break;
            }
        }
//	     $query = DB::getQueryLog();
//       $lastQuery = end($query);
	    // print_r($lastQuery);
//	     Log::info(': Error! query:'.vsprintf(str_replace('?', '`%s`', $lastQuery['query']), $lastQuery['bindings']));

        $signature_status = DB::table('lsg_aggregation')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id','=', 'lsg_aggregation.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->value('signature_status');

        if($request->ajax()){
            $data = DB::table('demand_requests')
            ->where('local_body_id',$lbdetails[0]->local_body_id)
            ->get(['demand_date','fund_yearmarked','amount_in_hand','fund_requested']);
            return Datatables::of($data)->make(true);
        }
        return view('Landholding/funddeclarations',compact('lbdetails','funddata','signature_status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // changes upcomming
        
        $userid=Auth::user()->id;

        $lbid = DB::table('local_body')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id','=', 'local_body.local_body_id')
        ->select('user_localbody_mapping.local_body_id')->where('user_localbody_mapping.user_id',$userid)
        ->get();

        $form_data = [
            'local_body_id'     => $lbid[0]->local_body_id,
            'demand_date'       => date("Y-m-d"),
            'fund_yearmarked'   => $request->yearmarkedfund,
            'amount_in_hand'    => $request->pendingfund,
            'fund_requested'    => $request->requestedfund,
            'created_at'        => date("Y-m-d H:i:s"),
            'updated_at'        => date("Y-m-d H:i:s"),
        ];

        $filters = [
            'fund_yearmarked'   =>  'trim|escape',
            'fund_requested'    =>  'trim|escape',
            'amount_in_hand'    =>  'trim|escape',
        ];
        
        $sanitizer  = new Sanitizer($form_data, $filters);
        $form_data = $sanitizer->sanitize();
        $valid_flag = 1;
        if($form_data['fund_requested'] <= 0){
            $valid_flag = 0;
        }
        if($valid_flag){
        DB::transaction(function () use ($form_data, $lbid) {
            $new_demand_id = DB::table('demand_requests')->insertGetId($form_data, 'demand_id');

            $workflow_demand_mapping_data = DB::table('landholding_workflow')
            ->join('landholding_beneficiaries', 'landholding_beneficiaries.beneficiary_id', '=', 'landholding_workflow.beneficiary_id')
            ->leftJoin('workflow_demand_mapping','workflow_demand_mapping.workflow_id','landholding_workflow.workflow_id')
            ->where('local_body_id',$lbid[0]->local_body_id)
            ->whereNull('workflow_demand_mapping.workflow_id')
            ->where('installment_release_status',0)
            ->get(['landholding_workflow.workflow_id'])
            ->toArray();

            foreach($workflow_demand_mapping_data as $data){
                $data->demand_id = $new_demand_id;
            }
            $workflow_demand_mapping_data= json_decode( json_encode($workflow_demand_mapping_data), true);
            DB::table('workflow_demand_mapping')->insert($workflow_demand_mapping_data);

        });
        }
        if($valid_flag)
            echo json_encode(['status'=>'success','message'=>'Updated successfully!']);
        else
            echo json_encode(['status'=>'warning','message'=>'Unchanged data!']);
        return redirect()->action('Landholding\FundDeclarationController@index');






    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
