<?php

namespace App\Http\Controllers\Landholding;
use App\Models\Landholding\LocalBodyTransfer;
use App\Models\Landholding\Beneficiary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Session;
use Auth;
use Carbon\Carbon;

class LocalbodyMigrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid=Auth::user()->id;
        $signature_status = DB::table('lsg_aggregation')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id','=', 'lsg_aggregation.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->value('signature_status');

        $local_bodies = DB::table('local_body')
        ->select('local_body_display_name','local_body_id')->orderBy('local_body_display_name')->get();

        if($request->ajax()){
            $lb_id=Session::get('lb_id');
            $data = LocalBodyTransfer::where('from_localbody',$lb_id)
            ->join('landholding_beneficiaries','landholding_beneficiaries.beneficiary_id','local_body_transfer.beneficiary_id')
            ->join('local_body','local_body.local_body_id','local_body_transfer.to_localbody')
            ->select([
                'local_body_transfer.beneficiary_id',
                'landholding_beneficiaries.beneficiary_name',
                'local_body.local_body_display_name',
                'local_body_transfer.status_flag',
                'local_body_transfer.created_at',
                'local_body_transfer.updated_at',
            ])->get();
            return Datatables::of($data)->make(true);
            }

        return view("Landholding/createtransferrequest",compact('local_bodies','signature_status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
   
        $lb_id=Session::get('lb_id');
       $beneficiary = DB::table('landholding_beneficiaries')
       ->select('beneficiary_name','addr_house_num','addr_house_name','addr_house_location','pincode','ration_number','aadhaar')
       ->where('local_body_id',$lb_id)
       ->where('beneficiary_id',$id)
       ->where('workflow_stage_id',2)
       ->get();
    //    ->toArray();
   
    if(count($beneficiary)>0)
    return json_encode(['beneficiaryData'=>$beneficiary]);
  else
    return json_encode(['status'=>'warning','message'=>'beneficiary not found']);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
       $lb_id=Session::get('lb_id');
       $formdata = [
           'beneficiary_id' => $id,
           'from_localbody' => $lb_id,
           'to_localbody'   => $request->lb_id,
            'status_flag'   => '0',
            'created_at' => date("Y-m-d H:i:s"),
       ];
       $insertflag = LocalBodyTransfer::insert($formdata);
       if($insertflag)
          return json_encode(['status'=>'success','message'=>'request created successfully!']);
        else
          return json_encode(['status'=>'warning','message'=>'failed !']);
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
