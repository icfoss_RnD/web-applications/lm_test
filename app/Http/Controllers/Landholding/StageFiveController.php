<?php

namespace App\Http\Controllers\Landholding;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Landholding\Beneficiary;
use App\Models\Landholding\LandholdingWorkflow;
use DB;
use DataTables;
use Auth;
use Waavi\Sanitizer\Sanitizer;
use Illuminate\Support\Facades\Log;

class StageFiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid=Auth::user()->id;

        $lbdetails = DB::table('local_body')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id', 'local_body.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->select('local_body_display_name','local_body.local_body_id')->get();
        
        $signature_status = DB::table('lsg_aggregation')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id','=', 'lsg_aggregation.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->value('signature_status');

        if($request->ajax()){
            $data = DB::table('landholding_beneficiaries')
            ->join('landholding_workflow', function($join){
                $join->on('landholding_workflow.beneficiary_id','landholding_beneficiaries.beneficiary_id')
                     ->on('landholding_workflow.workflow_stage_id','landholding_beneficiaries.workflow_stage_id');
            })
            ->where('local_body_id',$lbdetails[0]->local_body_id)
            ->where('landholding_beneficiaries.workflow_stage_id', 7) //stage 5
            ->get();
            return Datatables::of($data)->make(true);
            }
            return view('Landholding/stagefive',compact('signature_status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid=Auth::user()->id;
        $id=$request->id;
        // workflow columns
        $form_data = [
            'beneficiary_id'            =>$request->id,
            'installment_release_status'=>$request->released,
            'installment_date'          =>$request->instadate,
            'construction_status'       =>$request->completed,
            'working_days'              =>$request->workingdays,
            
        ];

        $filters = [
            'beneficiary_id' =>  'trim|escape',
            'installment_release_status' =>  'trim|escape',
            'installment_date' =>  'trim|escape',
            'construction_status' =>  'trim|escape',
        ];
        $sanitizer  = new Sanitizer($form_data, $filters);
        $form_data = $sanitizer->sanitize();

        $form_data['workflow_stage_id'] = 7;  //stage 5
        if($form_data['installment_release_status'] == 0 or empty($form_data['installment_date'])){
            $form_data['installment_release_status']=0;
            $form_data['installment_date']=null;
            $form_data['construction_status']=0;
        }
        $update_flag=LandholdingWorkflow::where('beneficiary_id','=',$id)
                        ->where('workflow_stage_id',7)
                        ->update($form_data);

        if($update_flag!=null or $workflow_flag!=null)
            return json_encode(['status'=>'success','message'=>'Updated successfully!']);
        else
            return json_encode(['status'=>'warning','message'=>'Unchanged data!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
