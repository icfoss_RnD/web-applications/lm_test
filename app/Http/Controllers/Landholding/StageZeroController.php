<?php

namespace App\Http\Controllers\Landholding;
use App\Models\Landholding\Beneficiary;
use App\Models\Landholding\LandholdingWorkflow;
use DB;
use DataTables;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Waavi\Sanitizer\Sanitizer;
use Illuminate\Support\Facades\Log;

class StageZeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid=Auth::user()->id;

        $lbdetails = DB::table('local_body')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id', 'local_body.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->select('local_body_display_name','local_body.local_body_id')->get();

        $signature_status = DB::table('lsg_aggregation')
        ->join('user_localbody_mapping','user_localbody_mapping.local_body_id','=', 'lsg_aggregation.local_body_id')
        ->where('user_localbody_mapping.user_id',$userid)
        ->value('signature_status');

        if($request->ajax()){
            $data = DB::table('landholding_beneficiaries')
            ->join('beneficiary_category','beneficiary_category.category_id','landholding_beneficiaries.category_id')
            ->where('local_body_id',$lbdetails[0]->local_body_id)
            ->where('landholding_beneficiaries.workflow_stage_id', 2)
            ->whereNotNull('resolution_date')
            ->whereNotNull('resolution_num')
            ->get();
            return Datatables::of($data)->make(true);
            }
            return view('Landholding/stagezero', compact('signature_status'));
        }

  /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // $beneficiary_relationship_check = 0; // fail
        //$beneficiary_relationship_name = $request->beneficiary_relationship;

        // if (!empty($request->beneficiary_relationship)) {
        //     if ($request->beneficiary_relationship != 'other') {
        //         $beneficiary_relationship_check = 1; //success
        //     } elseif ($request->beneficiary_relationship == 'other' && !empty($request->custom_relation)) {
        //         $beneficiary_relationship_name = 'other - ' . $request->custom_relation;
        //     }
        // }

        if ($request->beneficiary_relationship == 'other' && !empty($request->custom_relation)) {
            $beneficiary_relationship_name =  $request->custom_relation;
        }elseif($request->beneficiary_relationship == 'other' && empty($request->custom_relation))
        {
            $beneficiary_relationship_name = NULL;
        }
        else{
            $beneficiary_relationship_name = $request->beneficiary_relationship;
        }

    	$form_data = [
            'possession_verified'       => ($request->possession=='on' ? 1: 0),
            'possession_certificate'    => $request->possessioncertificate,
            'building_permit'           => $request->building_permit_date,
            'account_no'                => $request->account_number,
            'bank_name'                 => $request->bank_name,
            'branch_name'               => $request->branch_name,
            'ifsc_code'                 => $request->ifsc_code,
            'accountholder_name'        => $request->account_holder,
            'relation_with_beneficiary' => $beneficiary_relationship_name
        ];

        $filters = [
            'possession_verified'       => 'trim|escape',
            'possession_certificate'    => 'trim|escape',
            'building_permit'           => 'trim|escape',
            'account_no'                => 'trim|escape',
            'bank_name'                 => 'trim|escape',
            'branch_name'               => 'trim|escape',
            'ifsc_code'                 => 'trim|escape',
            'accountholder_name'        => 'trim|escape',
            'relation_with_beneficiary' => 'trim|escape'
        ];

        $sanitizer  = new Sanitizer($form_data, $filters);
        $form_data = $sanitizer->sanitize();

        // update all beneficiary data
        if(empty($form_data['account_no'])
        or empty($form_data['bank_name']) or empty($form_data['branch_name'])
        or empty($form_data['ifsc_code']) or empty($form_data['accountholder_name'])
        or empty($form_data['relation_with_beneficiary']))
        {
            $form_data['account_no']= NULL;
            $form_data['bank_name']= NULL;
            $form_data['branch_name']= NULL;
            $form_data['ifsc_code']= NULL;
            $form_data['accountholder_name']= NULL;
            $form_data['relation_with_beneficiary']= NULL;
             
        //     $update_flag=DB::table('landholding_beneficiaries')->where('beneficiary_id',$id)->update($form_data);
        }
        // else{
        $update_flag=DB::table('landholding_beneficiaries')->where('beneficiary_id',$id)->update($form_data);
        // }

          //     $update_flag=DB::table('landholding_beneficiaries')->where('beneficiary_id',$id)->update($form_data);



        // if 'possession_verified' is 1, update workflow stage id in _both_ tables 
        if($form_data['possession_verified'] == 1 && !empty($form_data['possession_certificate']) 
        && !empty($form_data['building_permit']) && !empty($form_data['account_no'])
        &&  !empty($form_data['bank_name']) && !empty($form_data['branch_name'])
        &&   !empty($form_data['ifsc_code']) && !empty($form_data['accountholder_name'])
        &&  !empty($form_data['relation_with_beneficiary'] ) 
        ){
                $form_data['workflow_stage_id'] = 3; //stage 1
                $stagezerodata=[
                    'beneficiary_id'    => $id,
                    'workflow_stage_id' => $form_data['workflow_stage_id'],
            ];
            $filters = [
                'beneficiary_id'    =>  'trim|escape',
            ];
            $sanitizer  = new Sanitizer($stagezerodata, $filters);
            $stagezerodata = $sanitizer->sanitize();

            $update_flag=DB::table('landholding_beneficiaries')->where('beneficiary_id',$id)->update($form_data);
            if (LandholdingWorkflow::where('beneficiary_id','=',$id)->exists()) {
                $insert_flag=LandholdingWorkflow::where('beneficiary_id','=',$id)->update($stagezerodata);
            }else{
                $update_flag = Log::error('StageZeroController: Beneficiary id '.$id.'. Inconsistent workflow detected.');
            }
        } else {
            $update_flag=$insert_flag=null;
        }

        if($update_flag or $insert_flag)
          return json_encode(['status'=>'success','message'=>'Updated successfully!']);
        else
          return json_encode(['status'=>'warning','message'=>'Unchanged data!']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}



//  
//              