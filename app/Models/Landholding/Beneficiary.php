<?php

namespace App\Models\Landholding;

use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{
    //
    protected $table = "landholding_beneficiaries";

    protected $primaryKey = "beneficiary_id";

    protected $fillable = [
        "local_body_id",
        "reg_id",
        "beneficiary_name",
        "addr_house_num",
        "addr_house_name",
        "addr_house_location",
        "pincode",
        "ration_number",
        "aadhaar",
        "gender",
        "category_id",
        "possession_verified",
        "resolution_date",
        "resolution_num",
        "approval_status",
        "workflow_stage_id"
    ];
}
