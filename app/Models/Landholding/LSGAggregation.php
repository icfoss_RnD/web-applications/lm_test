<?php

namespace App\Models\Landholding;

use Illuminate\Database\Eloquent\Model;

class LSGAggregation extends Model
{
	protected $table = "lsg_aggregation";

	protected $primaryKey = "lsg_aggr_id ";

	protected $fillable = [
    "local_body_id ",
	"beneficiary_count ",
	"comments",
	"signature_status ",
	"document_path ",
	"financial_year "
	];	
}
