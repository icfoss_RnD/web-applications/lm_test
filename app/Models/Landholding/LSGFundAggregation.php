<?php

namespace App\Models\Landholding;

use Illuminate\Database\Eloquent\Model;

class LSGFundAggregation extends Model
{
    //
	protected $table = "lsg_fund_aggregation";

	protected $primaryKey = "fund_aggregation_id";

	protected $fillable = [
    "local_body_id",
    "released_one",
    "released_two",
    "released_three",
    "released_four",
    "released_five",
    "pending_one",
    "pending_two",
    "pending_three",
    "pending_four",
    "pending_five",
    "fund_yearmarked",
    "fund_pending",
    "financial_year",
	];
}
