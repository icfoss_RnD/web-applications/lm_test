<?php

namespace App\Models\Landholding;

use Illuminate\Database\Eloquent\Model;

class LandholdingWorkflow extends Model
{
    protected $table = "landholding_workflow";

    protected $primaryKey = "workflow_id";

    protected $fillable = [
	"beneficiary_id",
	"agreement_date",
	"installment_release_status",
	"installment_date",
	"amount_released",
	"construction_status",
	"workflow_stage_id"
	];
     
}
