<?php

namespace App\Models\Landholding;

use Illuminate\Database\Eloquent\Model;

class LocalBodyTransfer extends Model
{
    protected $table = "local_body_transfer";

    protected $primaryKey = "transfer_id"; 

    protected $fillable = [
        "beneficiary_id",
        "from_localbody",
        "to_localbody",
        "status_flag",
    ];
       
}
