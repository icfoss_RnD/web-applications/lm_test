<?php

namespace App\Models\Landholding;

use Illuminate\Database\Eloquent\Model;

class NewApplicants extends Model
{
    protected $table = "new_applicants";

    protected $primaryKey = "applicant_id";

    protected $fillable = [
    "local_body_id",
    "applicant_name",
    "addr_house_num",
    "addr_house_name",
    "addr_house_location",
    "pincode",
    "ration_number",
    "aadhaar",
    "gender",
    "category_id",
    "applicant_category_id",
    "placeof_stay_id",
    "authentication_date",
    "reason_for_deferring",
    "minority",
    "handicapped",
    "women_lead",
    "fisherman",
    "plantation",
    "st_remote",
    "house_type",
    "damage_type",
    ];

}
