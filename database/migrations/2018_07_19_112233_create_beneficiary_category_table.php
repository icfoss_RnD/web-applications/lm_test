<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBeneficiaryCategoryTable extends Migration {

	public function up()
	{
		Schema::create('beneficiary_category', function(Blueprint $table) {
			$table->increments('category_id');
			$table->string('category_name', 15)->unique();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('beneficiary_category');
	}
}