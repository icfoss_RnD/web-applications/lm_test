<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandholdingBeneficiariesTable extends Migration {

	public function up()
	{
		Schema::create('landholding_beneficiaries', function(Blueprint $table) {
			$table->increments('beneficiary_id');
			$table->integer('local_body_id')->unsigned();
			$table->integer('reg_id')->unique();
			$table->string('beneficiary_name', 75);
			$table->string('addr_house_num', 75)->nullable();
			$table->string('addr_house_name', 75);
			$table->string('addr_house_location', 75);
			$table->integer('pincode');
			$table->bigInteger('ration_number')->index();
			$table->bigInteger('aadhaar')->default('0');
			$table->char('gender', 1);
			$table->integer('category_id')->unsigned();
			$table->boolean('possession_verified')->default(0);
			$table->date('resolution_date')->nullable();
			$table->string('resolution_num', 20)->nullable();
			$table->boolean('approval_status')->default(0);
			$table->integer('workflow_stage_id')->unsigned()->nullable();
			$table->boolean('minority');
			$table->boolean('handicapped');
			$table->boolean('women_lead');
			$table->boolean('fisherman');
			$table->boolean('plantation');
			$table->boolean('st_remote')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('landholding_beneficiaries');
	}
}