<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandholdingWorkflowStageTable extends Migration {

	public function up()
	{
		Schema::create('landholding_workflow_stage', function(Blueprint $table) {
			$table->increments('workflow_stage_id');
			$table->string('stage_name', 10)->unique();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('landholding_workflow_stage');
	}
}