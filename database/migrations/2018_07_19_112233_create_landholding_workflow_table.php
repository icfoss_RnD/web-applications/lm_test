<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandholdingWorkflowTable extends Migration {

	public function up()
	{
		Schema::create('landholding_workflow', function(Blueprint $table) {
			$table->increments('workflow_id');
			$table->integer('beneficiary_id')->unsigned();
			$table->date('agreement_date')->nullable();
			$table->boolean('installment_release_status')->default(0);
			$table->date('installment_date')->nullable();
			$table->float('amount_released')->nullable();
			$table->boolean('construction_status')->default(0);
			$table->integer('workflow_stage_id')->unsigned();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('landholding_workflow');
	}
}