<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocalBodyTable extends Migration {

	public function up()
	{
		Schema::create('local_body', function(Blueprint $table) {
			$table->increments('local_body_id');
			$table->string('local_body_name', 25);
			$table->string('local_body_display_name', 60);
			$table->string('lb_code', 10);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('local_body');
	}
}