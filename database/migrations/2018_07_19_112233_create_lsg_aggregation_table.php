<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLsgAggregationTable extends Migration {

	public function up()
	{
		Schema::create('lsg_aggregation', function(Blueprint $table) {
			$table->increments('lsg_aggr_id');
			$table->integer('local_body_id')->unsigned();
			$table->integer('beneficiary_count')->default('0');
			$table->text('comments')->nullable();
			$table->boolean('signature_status')->default(0);
			$table->longText('signed_data');
			$table->integer('financial_year')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('lsg_aggregation');
	}
}