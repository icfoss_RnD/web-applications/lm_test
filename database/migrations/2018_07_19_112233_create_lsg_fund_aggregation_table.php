<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLsgFundAggregationTable extends Migration {

	public function up()
	{
		Schema::create('lsg_fund_aggregation', function(Blueprint $table) {
			$table->increments('fund_aggregation_id');
			$table->integer('local_body_id')->unsigned();
			$table->integer('fund_yearmarked')->default('0');
			$table->integer('fund_pending')->default('0');
			$table->integer('financial_year');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('lsg_fund_aggregation');
	}
}