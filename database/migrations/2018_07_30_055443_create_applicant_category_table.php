<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantCategoryTable extends Migration {

	public function up()
	{
		Schema::create('applicant_category', function(Blueprint $table) {
			$table->increments('applicant_category_id');
			$table->string('category_name', 20)->unique();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('applicant_category');
	}
}