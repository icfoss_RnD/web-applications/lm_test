<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewApplicantsTable extends Migration {

	public function up()
	{
		Schema::create('new_applicants', function(Blueprint $table) {
			$table->integer('applicant_id')->unique();
			$table->integer('local_body_id')->unsigned();
			$table->string('applicant_name', 75);
			$table->string('addr_house_num', 75)->nullable();
			$table->string('addr_house_name', 75);
			$table->string('addr_house_location', 75);
			$table->integer('pincode');
			$table->bigInteger('ration_number')->unique()->nullable();
			$table->bigInteger('aadhaar')->unique()->nullable()->default('0');
			$table->char('gender', 1);
			$table->integer('category_id')->unsigned();
			$table->integer('applicant_category_id')->unsigned();
			$table->integer('placeof_stay_id')->unsigned();
			$table->date('authentication_date');
			$table->integer('reason_for_deferring')->unsigned();
			$table->boolean('minority');
			$table->boolean('handicapped');
			$table->boolean('women_lead');
			$table->boolean('fisherman');
			$table->boolean('plantation');
			$table->boolean('st_remote')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('life_mission_applicants');
	}
}
