<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePresentPlaceofStayTable extends Migration {

	public function up()
	{
		Schema::create('present_placeof_stay', function(Blueprint $table) {
			$table->increments('place_of_stay_id');
			$table->string('place_of_stay', 150);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('present_placeof_stay');
	}
}