<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReasonForDeferringTable extends Migration {

	public function up()
	{
		Schema::create('reason_for_deferring', function(Blueprint $table) {
			$table->increments('reason_id');
			$table->string('reason_for_deferring', 150);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('reason_for_deferring');
	}
}