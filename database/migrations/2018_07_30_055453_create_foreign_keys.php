<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('landholding_beneficiaries', function(Blueprint $table) {
			$table->foreign('local_body_id')->references('local_body_id')->on('local_body')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('landholding_beneficiaries', function(Blueprint $table) {
			$table->foreign('category_id')->references('category_id')->on('beneficiary_category')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('landholding_beneficiaries', function(Blueprint $table) {
			$table->foreign('workflow_stage_id')->references('workflow_stage_id')->on('landholding_workflow_stage')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('landholding_workflow', function(Blueprint $table) {
			$table->foreign('beneficiary_id')->references('beneficiary_id')->on('landholding_beneficiaries')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('landholding_workflow', function(Blueprint $table) {
			$table->foreign('workflow_stage_id')->references('workflow_stage_id')->on('landholding_workflow_stage')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('lsg_aggregation', function(Blueprint $table) {
			$table->foreign('local_body_id')->references('local_body_id')->on('local_body')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('user_localbody_mapping', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('user_localbody_mapping', function(Blueprint $table) {
			$table->foreign('local_body_id')->references('local_body_id')->on('local_body')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('lsg_fund_aggregation', function(Blueprint $table) {
			$table->foreign('local_body_id')->references('local_body_id')->on('lsg_aggregation')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('life_mission_applicants', function(Blueprint $table) {
			$table->foreign('category_id')->references('category_id')->on('beneficiary_category')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('life_mission_applicants', function(Blueprint $table) {
			$table->foreign('applicant_category_id')->references('applicant_category_id')->on('applicant_category')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('life_mission_applicants', function(Blueprint $table) {
			$table->foreign('placeof_stay_id')->references('place_of_stay_id')->on('present_placeof_stay')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('life_mission_applicants', function(Blueprint $table) {
			$table->foreign('reason_for_deferring')->references('reason_id')->on('reason_for_deferring')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('landholding_beneficiaries', function(Blueprint $table) {
			$table->dropForeign('landholding_beneficiaries_local_body_id_foreign');
		});
		Schema::table('landholding_beneficiaries', function(Blueprint $table) {
			$table->dropForeign('landholding_beneficiaries_category_id_foreign');
		});
		Schema::table('landholding_beneficiaries', function(Blueprint $table) {
			$table->dropForeign('landholding_beneficiaries_workflow_stage_id_foreign');
		});
		Schema::table('landholding_workflow', function(Blueprint $table) {
			$table->dropForeign('landholding_workflow_beneficiary_id_foreign');
		});
		Schema::table('landholding_workflow', function(Blueprint $table) {
			$table->dropForeign('landholding_workflow_workflow_stage_id_foreign');
		});
		Schema::table('lsg_aggregation', function(Blueprint $table) {
			$table->dropForeign('lsg_aggregation_local_body_id_foreign');
		});
		Schema::table('user_localbody_mapping', function(Blueprint $table) {
			$table->dropForeign('user_localbody_mapping_user_id_foreign');
		});
		Schema::table('user_localbody_mapping', function(Blueprint $table) {
			$table->dropForeign('user_localbody_mapping_local_body_id_foreign');
		});
		Schema::table('lsg_fund_aggregation', function(Blueprint $table) {
			$table->dropForeign('lsg_fund_aggregation_local_body_id_foreign');
		});
		Schema::table('life_mission_applicants', function(Blueprint $table) {
			$table->dropForeign('life_mission_applicants_category_id_foreign');
		});
		Schema::table('life_mission_applicants', function(Blueprint $table) {
			$table->dropForeign('life_mission_applicants_applicant_category_id_foreign');
		});
		Schema::table('life_mission_applicants', function(Blueprint $table) {
			$table->dropForeign('life_mission_applicants_placeof_stay_id_foreign');
		});
		Schema::table('life_mission_applicants', function(Blueprint $table) {
			$table->dropForeign('life_mission_applicants_reason_for_deferring_foreign');
		});
	}
}
