<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocalBodyTransferTable extends Migration {

	public function up()
	{
		Schema::create('local_body_transfer', function(Blueprint $table) {
			$table->increments('transfer_id');
			$table->integer('beneficiary_id')->unsigned();
			$table->integer('from_localbody');
			$table->integer('to_localbody');
			$table->tinyInteger('status_flag')->default('0');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('local_body_transfer');
	}
}
