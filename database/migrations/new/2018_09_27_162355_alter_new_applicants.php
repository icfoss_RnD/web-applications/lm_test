<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNewApplicants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
	Schema::table('new_applicants', function (Blueprint $table) {
	    $table->date('resolution_date')->nullable();
	    $table->char('resolution_num',20)->nullable();
	    $table->integer('beneficiary_id')->nullable();
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
	Schema::table('new_applicants', function (Blueprint $table) {
	    $table->dropColumn(['resolution_date', 'resolution_num', 'beneficiary_id']);
	});
    }
}
