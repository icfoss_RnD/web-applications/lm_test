<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
class ALocalBodyDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	
	$path = resource_path() .'/seeds/2.localbody.csv';
        $reader = Reader::createFromPath($path, 'r');
        $records = $reader->getRecords(['local_body_id','local_body_name','local_body_display_name','lb_code']);
	foreach ($records as $offset => $record) {
    	echo $offset."\n";
        DB::table('local_body')->insert([
            'local_body_id' => $record['local_body_id'],
            'local_body_name' => $record['local_body_name'],
	    'local_body_display_name'=>$record['local_body_display_name'],
	    'lb_code'=>$record['lb_code'],
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }
    }
}
