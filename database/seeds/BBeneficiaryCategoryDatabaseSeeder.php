<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
class BBeneficiaryCategoryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	$path = resource_path() .'/seeds/3.ben_category.csv';
        $reader = Reader::createFromPath($path, 'r');
        $records = $reader->getRecords(['category_id','category_name']);
	foreach ($records as $offset => $record) {
    	echo $offset."\n";
        DB::table('beneficiary_category')->insert([
            'category_id' => $record['category_id'],
            'category_name' => $record['category_name'],
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }
    }
}
