<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
class CLandholdingWorkflowStageDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	
	$path = resource_path() .'/seeds/4.workflow_stage.csv';
        $reader = Reader::createFromPath($path, 'r');
        $records = $reader->getRecords(['workflow_stage_id','stage_name']);
	foreach ($records as $offset => $record) {
    	echo $offset."\n";
        DB::table('landholding_workflow_stage')->insert([
            'workflow_stage_id' => $record['workflow_stage_id'],
            'stage_name' => $record['stage_name'],
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }
    }
}
