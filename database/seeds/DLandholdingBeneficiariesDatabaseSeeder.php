<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
class DLandholdingBeneficiariesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	
	$path = resource_path() .'/seeds/1.land_holding_beneficiaries.csv';
        $reader = Reader::createFromPath($path, 'r');
        $records = $reader->getRecords(['local_body_id','reg_id','beneficiary_name','addr_house_num','addr_house_name'
        ,'addr_house_location','pincode','ration_number','aadhaar','gender','category_id']);
	foreach ($records as $offset => $record) {
    	echo $offset."\n";
        DB::table('landholding_beneficiaries')->insert([
            'local_body_id' => $record['local_body_id'],
            'reg_id' => $record['reg_id'],
            'beneficiary_name' => $record['beneficiary_name'],
            'addr_house_num' => $record['addr_house_num'],
            'addr_house_name' => $record['addr_house_name'],
            'addr_house_location' => $record['addr_house_location'],
            'pincode' => $record['pincode'],
            'ration_number' => $record['ration_number'],
            'aadhaar' => $record['aadhaar'],
            'gender' => $record['gender'],
            'category_id' => $record['category_id'],
            'possession_verified' => 0,
            'resolution_date' => null,
            'resolution_num' => null,
            'approval_status' => 0,
            'workflow_stage_id' => 1,
	    'minority'=> 0,
            'handicapped'=> 0,
	    'women_lead'=> 0,
	    'fisherman'=> 0,
	    'plantation'=> 0,
            'st_remote'=> 0,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
	}
    }
}
