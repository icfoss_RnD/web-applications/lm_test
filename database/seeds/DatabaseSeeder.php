<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
	$this->call([
           ALocalBodyDatabaseSeeder::class,
           BBeneficiaryCategoryDatabaseSeeder::class,
           CLandholdingWorkflowStageDatabaseSeeder::class,
           DLandholdingBeneficiariesDatabaseSeeder::class,
           EUserTableSeeder::class,
           FRolesTableSeeder::class,
           GUserRolesDatabaseSeeder::class,
           HUserLocalbodyMappingTableSeeder::class,
    	]);
    }
}
