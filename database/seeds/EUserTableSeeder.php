<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
class EUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function generateStrongPassword($length = 10, $add_dashes = false, $available_sets = 'luds')
    {
    	$sets = array();
    	if(strpos($available_sets, 'l') !== false)
    		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
    	if(strpos($available_sets, 'u') !== false)
    		$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    	if(strpos($available_sets, 'd') !== false)
    		$sets[] = '23456789';
    	if(strpos($available_sets, 's') !== false)
    		$sets[] = '!$#%_@';
    	$all = '';
    	$password = '';
    	foreach($sets as $set)
    	{
    		$password .= $set[array_rand(str_split($set))];
    		$all .= $set;
    	}
    	$all = str_split($all);
    	for($i = 0; $i < $length - count($sets); $i++)
    		$password .= $all[array_rand($all)];
    	$password = str_shuffle($password);
    	if(!$add_dashes)
    		return $password;
    	$dash_len = floor(sqrt($length));
    	$dash_str = '';
    	while(strlen($password) > $dash_len)
    	{
    		$dash_str .= substr($password, 0, $dash_len) . '-';
    		$password = substr($password, $dash_len);
    	}
    	$dash_str .= $password;
    	return $dash_str;
    }


    public function run()
    {
        $file = fopen(resource_path()."/seeds/lifemission_users_pwd.csv","w");
        fputcsv($file,['username','password']);
    	$path = resource_path() .'/seeds/4.users.csv';
        $reader = Reader::createFromPath($path, 'r');
        $records = $reader->getRecords(['id','name','plain_password','password']);
	foreach ($records as $offset => $record) {
    	echo $offset."\n";
	$password = $this->generateStrongPassword();
        fputcsv($file,[$record['name'],$password]);
        DB::table('users')->insert([
	    'id' => $record['id'],
	    'name' => $record['name'],
	    'password' => bcrypt($password),
	    'created_at' => date("Y-m-d H:i:s"),
	    'updated_at' => date("Y-m-d H:i:s"),
        ]);
	}
	fclose($file);
    }
}
