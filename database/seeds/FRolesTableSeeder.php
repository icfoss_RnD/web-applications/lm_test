<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
class FRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
	$path = resource_path() .'/seeds/6.roles.csv';
        $reader = Reader::createFromPath($path, 'r');
        $records = $reader->getRecords(['id','name']);
	foreach ($records as $offset => $record) {
	echo $offset."\n";
        DB::table('roles')->insert([
	    'id' => $record['id'],
	    'name' => $record['name']
        ]);
    }
    }
}
