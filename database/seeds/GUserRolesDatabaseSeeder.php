<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
class GUserRolesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	$path = resource_path() .'/seeds/7.users_roles.csv';
        $reader = Reader::createFromPath($path, 'r');
        $records = $reader->getRecords(['role_id','user_id']);
	foreach ($records as $offset => $record) {
	echo $offset."\n";
        DB::table('user_has_roles')->insert([
	    'role_id' => $record['role_id'],
	    'user_id' => $record['user_id']
        ]);
    	}
    }
}
