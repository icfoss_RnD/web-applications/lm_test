<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
class HUserLocalbodyMappingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	$path = resource_path() .'/seeds/8.userlocalbody_map.csv';
        $reader = Reader::createFromPath($path, 'r');
        $records = $reader->getRecords(['user_id','local_body_id']);
	foreach ($records as $offset => $record) {
	echo $offset."\n";
        DB::table('user_localbody_mapping')->insert([
	     'user_id' => $record['user_id'],
	     'local_body_id' => $record['local_body_id']
        ]);
    	}
    }
}
