@extends('layouts.app') 
@section('title', 'Migrations')
 @section('breadcrumbs')
 <li>/ <a href="#">Local Body Migration </a>
    / <a href="#">Accept Requests</a>
</li>
@endsection
@section('content')
<div class="container" style="width:100%">
        <div class="row">
            <h3 class="meera">Local Body Migration Requests </h3>
        </div>
            <table id="example" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
            <thead>
                <tr>
                    <th>Beneficiary Id</th>
                    <th>Beneficiary Name</th>
                    <th>Local Body Name</th>
                </tr>
            </thead>
        </table>
      </div>
     </div>


 <!-- confirmTransfer Modal -->

    <div id="confirmTransfer" class="modal fade meera" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="height:500px;overflow-y: auto;">
      <div class="modal-header">
        <h3 class="meera">Initiate Transfer Request</h3>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container" style="width:100%">
          <div class="row">
            <form id="addForm" class="form form-horizontal" >
              {{ csrf_field() }}
              <input type="hidden" id="id" name="id" />
              <div class="form-group">
                <label class="col-sm-12 meera" for="name">ഗുണഭോക്താവിന്റെ പേര്:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="name" name="name"   disabled/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr">ഗുണഭോക്താവിന്റെ മേല്‍ വിലാസം:</label>
                <div class="col-sm-12">
                  <input type="addr" class="form-control" id="addr" name="addr"  disabled />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="ration">റേഷന്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input  type="text" class="form-control" id="ration" name="ration" disabled/>
                </div>
              </div>
	         <div class="form-group">
                <label class="col-sm-12 meera" for="adhaar">ആധാര്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="adhaar" name="adhaar" disabled/>
                </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="acceptTransferRequest">Accept Transfer Request</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
              
              </form>
            </div>
        </div>
        </div>
        </div>
        </div>
        </div>
    <!-- confirmTransfer Modal Ends-->

@push('bodyscripts')
<script>
$(document).ready(function(){
    var table = $('#example').DataTable({
        "processing": true,
        "serverSide": true,
        "searching": true,
        "ajax":
        {
        "url": APP_URL+"/landholding/accepttransferrequest",
        "type": "GET",
        "dataType":"json"
        },
        columns: [
            {data: 'beneficiary_id', name: 'beneficiary_id'},
            {data: 'beneficiary_name', name: 'beneficiary_name'},
            {data: 'local_body_display_name', name: 'local_body_display_name'},
            {data: 'transfer_id', name: 'transfer_id'},
            {data: 'from_localbody', name: 'from_localbody'},
            {data: 'to_localbody', name: 'to_localbody'},
            {data: 'status_flag', name: 'status_flag'},
        ],
        columnDefs: [
            {
            targets: [3,4,5,6],
            visible: false
            },
        ],
    });


 $('#example tbody').on( 'click', 'tr', function () {
        var row_data = table.row(this).data();
        $('#id').val(row_data.beneficiary_id);
        $('#name').val(row_data.beneficiary_name);
        $('#addr').val(row_data.addr_house_num + '/' +row_data.addr_house_name + ',' +row_data.addr_house_location +','+row_data. pincode);
        $('#ration').val(row_data.ration_number);
        $('#adhaar').val(row_data.aadhaar);
        $("#confirmTransfer").modal('show');
 
});

$('#acceptTransferRequest').on('click',function(){
        var benid = $('#id').val();
	$("#acceptTransferRequest").prop('disabled', true);
        $('#acceptTransferRequest').html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Accept');
        $.ajax({
            url: APP_URL+"/landholding/accepttransferrequest/"+benid,
            type: "PUT",
            dataType: "json",
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
                success: function(result){
                  $.toaster({
                    message : result.message,
                    title : 'Message',
                    priority : result.status,
                    settings: { timeout : 6000 },
                });
		$("#acceptTransferRequest").prop('disabled', false);
	        $('#acceptTransferRequest').html('Accept');
                $('#confirmTransfer').modal('toggle');
                table.ajax.reload();
               
            }
        });
  });

});

</script>
@endpush
@endsection
