@extends('layouts.app') @section('title', 'Beneficiaries') @section('breadcrumbs')
<li>/
  <a href="#"></a>
</li>
@endsection @section('content') @if($dateFlag == 1)
<div class="container col-lg-12" style="width:100%">
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-info alert-dismissible meera" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>നിർദേശങ്ങൾ :</strong>അധിക പട്ടികയിലേക്കും മാറ്റങ്ങൾക്കും അപേക്ഷകരെ ചേർക്കുന്നതിനുള്ള കാലാവതി കഴിഞ്ഞു
      </div>
    </div>
  </div>
</div>
<br> @else
<div class="container col-lg-12" style="width:100%">
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-danger alert-dismissible meera" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>നിർദേശങ്ങൾ :</strong>അധിക പട്ടികയിലേക്കും മാറ്റങ്ങൾക്കും അപേക്ഷകരെ ചേർക്കുന്നതിനുള്ള അവസാന തിയതി
        <strong>{{ $lastdate }}</strong>
      </div>
    </div>
  </div>
</div>

<div class="col-md-12">
  <button type="button" id="addapplicant" class="btn btn-info " style="float:right;">Add Applicants</button>
</div>
<br>
<br>

<div id="addFormModal" class="modal fade meera" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="height:500px;overflow-y: auto;">
      <div class="modal-header">
        <h3 class="meera"></h3>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container" style="width:100%">
          <div class="row">
            <form id="addForm" class="form form-horizontal" method="post">
              {{ csrf_field() }}
              <input type="hidden" id="id" name="id" />
              <input type="hidden" id="applicant_category_id" name="applicant_category_id" value='4' />
              <div class="form-group">
                <label class="col-sm-12 meera" for="name">അപേക്ഷകന്റെ പേര്:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="name" name="name" required />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr_house_num">വീട്ട് നമ്പർ:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="addr_house_num" name="addr_house_num" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr_house_name">വീട്ട് പേര്:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="addr_house_name" name="addr_house_name" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr_house_location">സ്ഥലം:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="addr_house_location" name="addr_house_location" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr_house_pincode">പിൻ കോഡ്::</label>
                <div class="col-sm-12">
                  <input type="text" onkeypress="isNumber(event)" class="form-control" id="addr_house_pincode" name="addr_house_pincode" maxlength="8"
                  />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="ration">റേഷന്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input onkeypress="isNumber(event)" maxlength="10" type="text" class="form-control" id="ration" name="ration" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="adhaar">ആധാര്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input onkeypress="isNumber(event)" maxlength="12" type="text" class="form-control" id="adhaar" name="adhaar" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="gender">ലിംഗ പദവി:</label>
                <div class="col-sm-12">
                  <select id="gender" name="gender" class="form-control">
                    <option value="M">പുരുഷൻ</option>
                    <option value="F">സ്ത്രീ</option>
                    <option value="T">ഭിന്നലിംഗം</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-12 meera" for="category">വിഭാഗം:</label>
                <div class="col-sm-12">
                  <select id="category" name="category" class="form-control meera">
                    <option value="1">പൊതുവിഭാഗം</option>
                    <option value="2">പട്ടികവര്‍ഗ്ഗം </option>
                    <option value="3">പട്ടികജാതി </option>
                    <option value="4">ഒ.ബി.സി </option>
                  </select>
                </div>
              </div>
              <div class="form-group subcategory">
                <input type="checkbox" class="" id="minority" name="minority" />
                <label class="meera" for="minority">മൈനോറിറ്റി </label>
                <input type="checkbox" class="" id="handicapped" name="handicapped" />
                <label class="meera" for="handicapped">അംഗപരിമിതർ</label>
                <input type="checkbox" class="" id="womenlead" name="womenlead" />
                <label class="meera" for="womenlead">സ്ത്രീ കേന്ദ്രീത കുടുംബം</label>
                <br>
                <input type="checkbox" class="" id="fisherman" name="fisherman" />
                <label class="meera" for="fisherman">മത്സ്യ ബന്ധനത്തൊഴിലാളി</label>
                <input type="checkbox" class="" id="plantation" name="plantation" />
                <label class="meera" for="plantation">പ്ലാന്റേഷന്‍ തൊഴിലാളി</label>
                </br>
                <input type="checkbox" class="" id="st_remote" name="st_remote" />
                <label class="meera" for="st_remote">വിദൂര സങ്കേതങ്ങളിലെ പട്ടികവര്‍ഗ്ഗം </label>
              </div>

              <div class="form-group">
                <label class="col-sm-12 meera" for="house_type">വീടിന്റെ തരം: </label>
                <div class="col-sm-12">
                  <select id="house_type" name="house_type" class="form-control meera">
                    <option value="1">നിലവിൽ താമസിച്ചിരുന്ന പഴയ വീട്</option>
                    <option value="2">LIFE ധനസഹായത്തോടെ പൂർത്തിയാക്കിയ / പൂർത്തിയാക്കികൊണ്ടിരുന്ന വീട്</option>
                    <option value="3">LIFE ധനസഹായത്തോടെ നിർമ്മിച്ച  / നിർമ്മിച്ചു കൊണ്ടിരുന്ന വീട്</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-12 meera" for="damage_type">കേടുപാടിന്റെ തരം: </label>
                <div class="col-sm-12">
                  <select id="damage_type" name="damage_type" class="form-control meera">
                    <option value="1">വീടും ഭൂമിയും നഷ്ടപ്പെട്ടു</option>
                    <option value="2">വീട് പൂർണ്ണമായും തകർന്നു</option>
                    <option value="3">വീട് ഭാഗികമായി തകർന്നു</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- edit modal -->
<div id="editFormModal" class="modal fade meera" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="height:500px;overflow-y: auto;">
      <div class="modal-header">
        <h3 class="meera">EDIT APPLICANT</h3>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container" style="width:100%">
          <div class="row">
            <form id="editForm" class="form form-horizontal" method="post">
              {{ csrf_field() }}
              <input type="hidden" id="applicant_category_id" name="applicant_category_id" value='4' />
              <input type="hidden" id="id" name="id" />
              <div class="form-group">
                <label class="col-sm-12 meera" for="name">അപേക്ഷകന്റെ പേര്:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="Ename" name="name" disabled />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr">അപേക്ഷകന്റെ മേല്‍ വിലാസം:</label>
                <div class="col-sm-12">
                  <input type="addr" class="form-control" id="addr" name="addr" disabled />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="ration">റേഷന്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input onkeypress="isNumber(event)" maxlength="10" type="text" class="form-control" id="Eration" name="ration" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="adhaar">ആധാര്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input onkeypress="isNumber(event)" maxlength="12" type="text" class="form-control" id="Eadhaar" name="adhaar" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="gender">ലിംഗ പദവി:</label>
                <div class="col-sm-12">
                  <select id="Egender" name="gender" class="form-control">
                    <option value="M">പുരുഷൻ</option>
                    <option value="F">സ്ത്രീ</option>
                    <option value="T">ഭിന്നലിംഗം</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-12 meera" for="category">വിഭാഗം:</label>
                <div class="col-sm-12">
                  <select id="Ecategory" name="category" class="form-control meera">
                    <option value="1">പൊതുവിഭാഗം</option>
                    <option value="2">പട്ടികവര്‍ഗ്ഗം </option>
                    <option value="3">പട്ടികജാതി </option>
                    <option value="4">ഒ.ബി.സി </option>
                  </select>
                </div>
              </div>
              <div class="form-group subcategory">
                <input type="checkbox" class="" id="Eminority" name="minority" />
                <label class="meera" for="Eminority">മൈനോറിറ്റി </label>
                <input type="checkbox" class="" id="Ehandicapped" name="handicapped" />
                <label class="meera" for="Ehandicapped">അംഗപരിമിതർ</label>
                <input type="checkbox" class="" id="Ewomenlead" name="womenlead" />
                <label class="meera" for="Ewomenlead">സ്ത്രീ കേന്ദ്രീത കുടുംബം</label>
                <br>
                <input type="checkbox" class="" id="Efisherman" name="fisherman" />
                <label class="meera" for="Efisherman">മത്സ്യ ബന്ധനത്തൊഴിലാളി</label>
                <input type="checkbox" class="" id="Eplantation" name="plantation" />
                <label class="meera" for="Eplantation">പ്ലാന്റേഷന്‍ തൊഴിലാളി</label>
                </br>
                <input type="checkbox" class="" id="Est_remote" name="st_remote" />
                <label class="meera" for="Est_remote">വിദൂര സങ്കേതങ്ങളിലെ പട്ടികവര്‍ഗ്ഗം </label>
              </div>

              <div class="form-group">
                <label class="col-sm-12 meera" for="Ehouse_type">വീടിന്റെ തരം: </label>
                <div class="col-sm-12">
                  <select id="Ehouse_type" name="house_type" class="form-control meera">
                    <option value="1">നിലവിൽ താമസിച്ചിരുന്ന പഴയ വീട്</option>
                    <option value="2">LIFE ധനസഹായത്തോടെ പൂർത്തിയാക്കിയ / പൂർത്തിയാക്കികൊണ്ടിരുന്ന വീട്</option>
                    <option value="3">LIFE ധനസഹായത്തോടെ നിർമ്മിച്ച  / നിർമ്മിച്ചു കൊണ്ടിരുന്ന വീട്</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-12 meera" for="Edamage_type">കേടുപാടിന്റെ തരം: </label>
                <div class="col-sm-12">
                  <select id="Edamage_type" name="damage_type" class="form-control meera">
                    <option value="1">വീടും ഭൂമിയും നഷ്ടപ്പെട്ടു</option>
                    <option value="2">വീട് പൂർണ്ണമായും തകർന്നു</option>
                    <option value="3">വീട് ഭാഗികമായി തകർന്നു</option>
                  </select>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="edit">Save</button>
        <button type="button" class="btn btn-danger " id="delete">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endif
<div class="col-md-12">
  <h3 class="meera">2018ലെ മഴക്കെടുതിയിൽ വീട് നഷ്ടമായ LIFE ഗുണഭോക്താക്കൾ</h3>
  <table id="example" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
    <thead>
      <tr>
        <th>Applicant Id</th>
        <th>Name of Applicant</th>
        <th>Address</th>
        <th>Ration Card </th>
        <th>Aadhaar </th>
        <th>Gender</th>
        <th>Category </th>
        <th>Type of house</th>
        <th>Type of damage</th>
      </tr>
    </thead>
  </table>
</div>

@push('bodyscripts')
<script>
  $(document).ready(function () {
    $('#addapplicant').on('click', function () {
      $('#addFormModal').modal('toggle');
    });
    $('#authenticationdate').datetimepicker({
      lang: 'en',
      timepicker: false,
      minDate: '2017/01/01',
      maxDate: '0',
      format: 'Y-m-d',
      allowBlank: true,
      scrollInput: false
    });

    var id = 4;     //applicant category id
    var table = $('#example').DataTable({
      "processing": true,
      "serverSide": true,
      "searching": true,
      "ajax":
      {
        "url": APP_URL + "/landholding/category/4/applicants",
        "dataType": "json"
      },
      columns: [
        { data: 'applicant_id', name: 'applicant_id' },                       //0
        { data: 'applicant_name', name: 'applicant_name' },                   //1
        { data: 'addr_house_num', name: 'addr_house_num' },                   //2
        { data: 'ration_number', name: 'ration_number' },                     //3
        { data: 'aadhaar', name: 'aadhaar' },                                 //4
        { data: 'gender', name: 'gender' },                                   //5
        { data: 'category_name', name: 'category_name' },                     //6
        { data: 'house_type', name: 'house_type' },                           //7
        { data: 'damage_type', name: 'damage_type' },                         //8
        { data: 'applicant_category_id', name: 'applicant_category_id' },     //9
        { data: 'minority', name: 'minority' },                               //10
        { data: 'handicapped', name: 'handicapped' },                         //11
        { data: 'women_lead', name: 'women_lead' },                           //12
        { data: 'fisherman', name: 'fisherman' },                             //13
        { data: 'plantation', name: 'plantation' },                           //14
        { data: 'authentication_date', name: 'authentication_date' },         //15
        { data: 'reason_for_deferring', name: 'reason_for_deferring' },       //16
        { data: 'pincode', name: 'pincode' },                                 //17

      ],
      columnDefs: [
        {
          render: function (data, type, row, meta) {
            return data + '/\n' + row.addr_house_name + ',\n' + row.addr_house_location + ',\n' + row.pincode
          },
          targets: 2
        },
        {
          targets: 7,
          render: function (data, type, row, meta) {
            switch(data){
              case '1': return 'നിലവിൽ താമസിച്ചിരുന്ന പഴയ വീട്';
              case '2': return 'LIFE ധനസഹായത്തോടെ പൂർത്തിയാക്കിയ / പൂർത്തിയാക്കികൊണ്ടിരുന്ന വീട്';
              case '3': return 'LIFE ധനസഹായത്തോടെ നിർമ്മിച്ച  / നിർമ്മിച്ചു കൊണ്ടിരുന്ന വീട്';
              default: return data;
            }
          },
        },
        {
          targets: 8,
          render: function (data, type, row, meta) {
            switch(data){
              case '1': return 'വീടും ഭൂമിയും നഷ്ടപ്പെട്ടു';
              case '2': return 'വീട് പൂർണ്ണമായും തകർന്നു';
              case '3': return 'വീട് ഭാഗികമായി തകർന്നു';
              default: return data;
            }
          },
        },
        {
          targets: 9,
          visible: false
        },
        {
          targets: 10,
          visible: false
        },
        {
          targets: 11,
          visible: false
        },
        {
          targets: 12,
          visible: false
        },
        {
          targets: 13,
          visible: false
        },
        {
          targets: 14,
          visible: false
        },
        {
          targets: 15,
          visible: false
        },
        {
          targets: 16,
          visible: false
        },
        {
          targets: 17,
          visible: false
        }
      ],
    });

    $('#save').on('click', function () {
      var form_data = $('#addForm').serialize();
      $("#save").prop('disabled', true);
      $('#save').html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Save');
      $.ajax({
        url: APP_URL + "/landholding/category/4/applicants",
        type: "POST",
        dataType: "json",
        data: form_data,
        success: function (result) {
          $.toaster({
            message: result.message,
            title: 'Message',
            priority: result.status,
            settings: { timeout: 6000 },
          });
          $("#save").prop('disabled', false);
	        $('#save').html('Save');
          $('#addForm')[0].reset();
          $('#addFormModal').modal('toggle');
          table.ajax.reload();
        }
      });
    });

    $('#example tbody').on('click', 'tr', function () {
      var row_data = table.row(this).data();
      // var con=confirm("Are you sure you want to delet this ")
      $('#id').val(row_data.applicant_id);
      $('#Ename').val(row_data.applicant_name);
      $('#addr').val(row_data.addr_house_num + '/' + row_data.addr_house_name + ',' + row_data.addr_house_location + ',' + row_data.pincode);
      $('#Eration').val(row_data.ration_number);
      $('#Eadhaar').val(row_data.aadhaar);
      $('#Egender').val(row_data.gender);
      $('#Ecategory').val(row_data.category_id);
      $('#editauthenticationdate').val(row_data.authentication_date);
      if (row_data.minority == 1)
        $('#Eminority').prop("checked", true);
      else
        $("#Eminority").prop("checked", false);   //put else condition coz,unchecked not get activated when value it is 0
      if (row_data.handicapped == 1)
        $('#Ehandicapped').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
      else
        $("#Ehandicapped").prop("checked", false);
      if (row_data.women_lead == 1)
        $('#Ewomenlead').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
      else
        $("#Ewomenlead").prop("checked", false);
      if (row_data.fisherman == 1)
        $('#Efisherman').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
      else
        $("#Efisherman").prop("checked", false);
      if (row_data.plantation == 1)
        $('#Eplantation').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
      else
        $("#Eplantation").prop("checked", false);
      if (row_data.st_remote == 1)
        $('#Est_remote').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
      else
        $("#Est_remote").prop("checked", false);

      $('#Ehouse_type').val(row_data.house_type);
      $('#Edamage_type').val(row_data.damage_type);

      $('#editFormModal').modal('toggle');
    });

    $('#edit').on('click', function () {
      var form_data = $('#editForm').serialize();
      var id = $('#id').val();
      $("#edit").prop('disabled', true);
      $('#edit').html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Save');
      $.ajax({
        url: APP_URL + "/landholding/category/4/applicants/" + id,
        type: "PUT",
        dataType: "json",
        data: form_data,
        success: function (result) {
          $.toaster({
            message: result.message,
            title: 'Message',
            priority: result.status,
            settings: { timeout: 6000 },
          });
          $("#edit").prop('disabled', false);
	        $('#edit').html('Save');
          $('#editForm')[0].reset();
          $('#editFormModal').modal('toggle');
          table.ajax.reload();
        }
      });
    });

    $('#delete').on('click', function () {
      var form_data = $('#addForm').serialize();
      var id = $('#id').val();
      var con = confirm("Are you sure you want to delete this ")
      if (con == true) {
        $.ajax({
          url: APP_URL + "/landholding/category/4/applicants/" + id,
          type: "DELETE",
          dataType: "json",
          data: form_data,
          success: function (result) {
            $.toaster({
              message: result.message,
              title: 'Message',
              priority: result.status,
              settings: { timeout: 6000 },
            });
            $('#addForm')[0].reset();
            $('#editFormModal').modal('toggle');
            table.ajax.reload();
          }
        });
      }
    });


  }); //end of document.ready function
  function isNumber(event) {
    var inputValue = event.which;
    // allow numbers and null (0), backspace (8), del(127) .
    if (!(inputValue > 47 && inputValue < 58) && (inputValue != 0 && inputValue != 8 && inputValue != 127)) {
      event.preventDefault();
    }
  }


</script> @endpush @endsection
