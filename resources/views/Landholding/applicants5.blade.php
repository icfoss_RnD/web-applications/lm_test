@extends('layouts.app') @section('title', 'Beneficiaries') @section('breadcrumbs')
<li>/
  <a href="#"></a>
</li>
@endsection @section('content') @if($dateFlag == 1)
<div class="container col-lg-12" style="width:100%">
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-info alert-dismissible meera" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>നിർദേശങ്ങൾ :</strong>അധിക പട്ടികയിലേക്കും മാറ്റങ്ങൾക്കും അപേക്ഷകരെ ചേർക്കുന്നതിനുള്ള കാലാവതി കഴിഞ്ഞു
      </div>
    </div>
  </div>
</div>
<br> @else
<div class="container col-lg-12" style="width:100%">
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-danger alert-dismissible meera" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>നിർദേശങ്ങൾ :</strong>അധിക പട്ടികയിലേക്കും മാറ്റങ്ങൾക്കും അപേക്ഷകരെ ചേർക്കുന്നതിനുള്ള അവസാന തിയതി
        <strong>{{ $lastdate }}</strong>
      </div>
    </div>
  </div>
</div>

<div class="col-md-12">
  <button type="button" id="addapplicant" class="btn btn-info " style="float:right;">Add Applicants</button>
</div>
<br>
<br>

<div id="addFormModal" class="modal fade meera" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="height:500px;overflow-y: auto;">
      <div class="modal-header">
        <h3 class="meera"></h3>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container" style="width:100%">
          <div class="row">
            <form id="addForm" class="form form-horizontal" method="post">
              {{ csrf_field() }}
              <input type="hidden" id="id" name="id" />
              <input type="hidden" id="applicant_category_id" name="applicant_category_id" value='5' />
              <div class="form-group">
                <label class="col-sm-12 meera" for="name">അപേക്ഷകന്റെ പേര്:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="name" name="name" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr_house_num">വീട്ട് നമ്പർ:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="addr_house_num" name="addr_house_num" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr_house_name">വീട്ട് പേര്:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="addr_house_name" name="addr_house_name" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr_house_location">സ്ഥലം:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="addr_house_location" name="addr_house_location" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr_house_pincode">പിൻ കോഡ്:</label>
                <div class="col-sm-12">
                  <input type="text" onkeypress="isNumber(event)" class="form-control" id="addr_house_pincode" name="addr_house_pincode" maxlength="8"
                  />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="ration">റേഷന്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input onkeypress="isNumber(event)" maxlength="10" type="text" class="form-control" id="ration" name="ration" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="adhaar">ആധാര്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input onkeypress="isNumber(event)" maxlength="12" type="text" class="form-control" id="adhaar" name="adhaar" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="gender">ലിംഗ പദവി:</label>
                <div class="col-sm-12">
                  <select id="gender" name="gender" class="form-control">
                    <option value="M">പുരുഷൻ</option>
                    <option value="F">സ്ത്രീ</option>
                    <option value="T">ഭിന്നലിംഗം</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-12 meera" for="category">വിഭാഗം:</label>
                <div class="col-sm-12">
                  <select id="category" name="category" class="form-control meera">
                    <option value="1">പൊതുവിഭാഗം</option>
                    <option value="2">പട്ടികവര്‍ഗ്ഗം </option>
                    <option value="3">പട്ടികജാതി </option>
                    <option value="4">ഒ.ബി.സി </option>
                  </select>
                </div>
              </div>
              <div class="form-group subcategory">
                <input type="checkbox" class="" id="minority" name="minority" />
                <label class="meera" for="minority">മൈനോറിറ്റി </label>
                <input type="checkbox" class="" id="handicapped" name="handicapped" />
                <label class="meera" for="handicapped">അംഗപരിമിതർ</label>
                <input type="checkbox" class="" id="womenlead" name="womenlead" />
                <label class="meera" for="womenlead">സ്ത്രീ കേന്ദ്രീത കുടുംബം</label>
                <br>
                <input type="checkbox" class="" id="fisherman" name="fisherman" />
                <label class="meera" for="fisherman">മത്സ്യ ബന്ധനത്തൊഴിലാളി</label>
                <input type="checkbox" class="" id="plantation" name="plantation" />
                <label class="meera" for="plantation">പ്ലാന്റേഷന്‍ തൊഴിലാളി</label>
                </br>
                <input type="checkbox" class="" id="st_remote" name="st_remote" />
                <label class="meera" for="st_remote">വിദൂര സങ്കേതങ്ങളിലെ പട്ടികവര്‍ഗ്ഗം </label>
              </div>

              <label class="col-sm-12 meera" for="category">ഇപ്പോഴത്തെ താമസ സ്ഥലം:</label>
              <div class="form-group subcategory">
                <input type="radio" class="" id="placeofstay1" name="placeofstay" value="1" />
                <label class="meera" for="placeofstay1">കുടുംബവീട്ടിൽ മറ്റ് സഹോദര കുടുംബങ്ങളോടൊപ്പം </label>
                <br>
                <input type="radio" class="" id="placeofstay2" name="placeofstay" value="2" />
                <label class="meera" for="placeofstay2">ബന്ധുവീട്ടിൽ</label>
                <input type="radio" class="" id="placeofstay3" name="placeofstay" value="3" />
                <label class="meera" for="placeofstay3">സഹോദരങ്ങളുടെ വീട്ടിൽ</label>
                <input type="radio" class="" id="placeofstay4" name="placeofstay" value="4" />
                <label class="meera" for="placeofstay4">വാടകവീട്ടിൽ</label>
                <br>
                <input type="radio" class="" id="placeofstay5" name="placeofstay" value="5" />
                <label class="meera" for="placeofstay5">സ്വന്തമായുള്ള വാസയോഗ്യമല്ലാത്ത വീട്ടിൽ</label>
                <input type="radio" class="" id="placeofstay6" name="placeofstay" value="6" />
                <label class="meera" for="placeofstay6">പൊതുസ്ഥലത്ത് </label>
              </div>

              <label class="col-sm-12 meera" for="deferred">Reason for Deferrring</label>
              <div class="form-group subcategory">
                <input type="radio" class="" id="reason1" name="reason" value="1" />
                <label class="meera" for="">റേഷൻ കാർഡ് ഇല്ലാത്തത് </label>
                <br>
                <input type="radio" class="" id="reason2" name="reason" value="2" />
                <label class="meera" for="">പേരുൾപ്പെട്ട റേഷൻകാർഡിലുള്ളവരിൽ മറ്റൊരാൾക്ക് വീടുള്ളത്</label>
                <br>
                <input type="radio" class="" id="reason3" name="reason" value="3" />
                <label class="meera" for="">സ്വന്തമായി ഭൂമിയില്ലാത്തത്</label>
                <input type="radio" class="" id="reason4" name="reason" value="4" />
                <label class="meera" for="">മറ്റുള്ളവ</label>
                <br>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- edit modal -->
<div id="editFormModal" class="modal fade meera" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="height:500px;overflow-y: auto;">
      <div class="modal-header">
        <h3 class="meera">EDIT APPLICANT</h3>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container" style="width:100%">
          <div class="row">
            <form id="editForm" class="form form-horizontal" method="post">
              {{ csrf_field() }}
              <input type="hidden" id="applicant_category_id" name="applicant_category_id" value='5' />
              <input type="hidden" id="id" name="id" />
              <div class="form-group">
                <label class="col-sm-12 meera" for="name">അപേക്ഷകന്റെ പേര്:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="Ename" name="name" disabled />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr">അപേക്ഷകന്റെ മേല്‍ വിലാസം:</label>
                <div class="col-sm-12">
                  <input type="addr" class="form-control" id="addr" name="addr" disabled />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="ration">റേഷന്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input onkeypress="isNumber(event)" maxlength="10" type="text" class="form-control" id="Eration" name="ration" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="adhaar">ആധാര്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input onkeypress="isNumber(event)" maxlength="12" type="text" class="form-control" id="Eadhaar" name="adhaar" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="gender">ലിംഗ പദവി:</label>
                <div class="col-sm-12">
                  <select id="Egender" name="gender" class="form-control">
                    <option value="M">പുരുഷൻ</option>
                    <option value="F">സ്ത്രീ</option>
                    <option value="T">ഭിന്നലിംഗം</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-12 meera" for="category">വിഭാഗം:</label>
                <div class="col-sm-12">
                  <select id="Ecategory" name="category" class="form-control meera">
                    <option value="1">പൊതുവിഭാഗം</option>
                    <option value="2">പട്ടികവര്‍ഗ്ഗം </option>
                    <option value="3">പട്ടികജാതി </option>
                    <option value="4">ഒ.ബി.സി </option>
                  </select>
                </div>
              </div>
              <div class="form-group subcategory">
                <input type="checkbox" class="" id="Eminority" name="minority" />
                <label class="meera" for="minority">മൈനോറിറ്റി </label>
                <input type="checkbox" class="" id="Ehandicapped" name="handicapped" />
                <label class="meera" for="handicapped">അംഗപരിമിതർ</label>
                <input type="checkbox" class="" id="Ewomenlead" name="womenlead" />
                <label class="meera" for="womenlead">സ്ത്രീ കേന്ദ്രീത കുടുംബം</label>
                <br>
                <input type="checkbox" class="" id="Efisherman" name="fisherman" />
                <label class="meera" for="fisherman">മത്സ്യ ബന്ധനത്തൊഴിലാളി</label>
                <input type="checkbox" class="" id="Eplantation" name="plantation" />
                <label class="meera" for="plantation">പ്ലാന്റേഷന്‍ തൊഴിലാളി</label>
                </br>
                <input type="checkbox" class="" id="Est_remote" name="st_remote" />
                <label class="meera" for="st_remote">വിദൂര സങ്കേതങ്ങളിലെ പട്ടികവര്‍ഗ്ഗം </label>
              </div>

              <label class="col-sm-12 meera" for="category">ഇപ്പോഴത്തെ താമസ സ്ഥലം:</label>
              <div class="form-group subcategory">
                <input type="radio" class="" id="Eplaceofstay1" name="placeofstay" value="1" />
                <label class="meera" for="minority">കുടുംബവീട്ടിൽ മറ്റ് സഹോദര കുടുംബങ്ങളോടൊപ്പം </label>
                <br>
                <input type="radio" class="" id="Eplaceofstay2" name="placeofstay" value="2" />
                <label class="meera" for="handicapped">ബന്ധുവീട്ടിൽ</label>
                <input type="radio" class="" id="Eplaceofstay3" name="placeofstay" value="3" />
                <label class="meera" for="womenlead">സഹോദരങ്ങളുടെ വീട്ടിൽ</label>
                <input type="radio" class="" id="Eplaceofstay4" name="placeofstay" value="4" />
                <label class="meera" for="fisherman">വാടകവീട്ടിൽ</label>
                <br>
                <input type="radio" class="" id="Eplaceofstay5" name="placeofstay" value="5" />
                <label class="meera" for="plantation">സ്വന്തമായുള്ള വാസയോഗ്യമല്ലാത്ത വീട്ടിൽ</label>
                <input type="radio" class="" id="Eplaceofstay6" name="placeofstay" value="6" />
                <label class="meera" for="publicplace">പൊതുസ്ഥലത്ത് </label>
              </div>


              <label class="col-sm-12 meera" for="deferred">Reason for Deferrring</label>
              <div class="form-group subcategory">
                <input type="radio" class="" id="Ereason1" name="reason" value="1" />
                <label class="meera" for="">റേഷൻ കാർഡ് ഇല്ലാത്തത് </label>
                <br>
                <input type="radio" class="" id="Ereason2" name="reason" value="2" />
                <label class="meera" for="">പേരുൾപ്പെട്ട റേഷൻകാർഡിലുള്ളവരിൽ മറ്റൊരാൾക്ക് വീടുള്ളത്</label>
                <br>
                <input type="radio" class="" id="Ereason3" name="reason" value="3" />
                <label class="meera" for="">സ്വന്തമായി ഭൂമിയില്ലാത്തത്</label>
                <input type="radio" class="" id="Ereason4" name="reason" value="4" />
                <label class="meera" for="">മറ്റുള്ളവ</label>
                <br>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="edit">Save</button>
        <button type="button" class="btn btn-danger " id="delete">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endif
<div class="col-md-12">
  <h3 class="meera">ഗുണഭോക്തൃ പട്ടികയിൽ പേരുണ്ടെങ്കിലും അർഹത തെളിയിച്ചിട്ടില്ലാത്തവർ</h3>
  <table id="example" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
    <thead>
      <tr>
        <th>Applicant Id</th>
        <th>Name of Applicant</th>
        <th>Address</th>
        <th>Ration Card Number</th>
        <th>Aadhaar Number</th>
        <th>Gender</th>
        <th>Category </th>
        <th>Present Place of stay</th>
      </tr>
    </thead>
  </table>
</div>

@push('bodyscripts')
<script>
  $(document).ready(function () {
    $('#addapplicant').on('click', function () {
      $('#addFormModal').modal('toggle');
    });
    $('#authenticationdate').datetimepicker({
      lang: 'en',
      timepicker: false,
      minDate: '2017/01/01',
      maxDate: '0',
      format: 'Y-m-d',
      allowBlank: true,
      scrollInput: false
    });

    var id = 5;     //applicant category id
    var table = $('#example').DataTable({
      "processing": true,
      "serverSide": true,
      "searching": true,
      "ajax":
      {
        "url": APP_URL + "/landholding/category/5/applicants",
        "type": "GET",
        "dataType": "json"
      },
      columns: [
        { data: 'applicant_id', name: 'applicant_id' },                       //0
        { data: 'applicant_name', name: 'applicant_name' },                   //1
        { data: 'addr_house_num', name: 'addr_house_num' },                   //2
        { data: 'ration_number', name: 'ration_number' },                     //3
        { data: 'aadhaar', name: 'aadhaar' },                                 //4
        { data: 'gender', name: 'gender' },                                   //5
        { data: 'category_name', name: 'category_name' },         //6
        { data: 'place_of_stay', name: 'placeof_stay_id.placeof_stay' },                 //7
        { data: 'applicant_category_id', name: 'applicant_category_id' },     //8
        { data: 'minority', name: 'minority' },                               //9
        { data: 'handicapped', name: 'handicapped' },                         //10
        { data: 'women_lead', name: 'women_lead' },                           //11
        { data: 'fisherman', name: 'fisherman' },                             //12
        { data: 'plantation', name: 'plantation' },                           //13
        { data: 'authentication_date', name: 'authentication_date' },         //14
        { data: 'reason_for_deferring', name: 'reason_for_deferring' },       //15
        { data: 'pincode', name: 'pincode' },     //16

      ],
      columnDefs: [
        {
          render: function (data, type, row, meta) {
            return data + '/\n' + row.addr_house_name + ',\n' + row.addr_house_location + ',\n' + row.pincode
          },
          targets: 2
        },
        {
          targets: 8,
          visible: false
        },
        {
          targets: 9,
          visible: false
        },
        {
          targets: 10,
          visible: false
        },
        {
          targets: 11,
          visible: false
        },
        {
          targets: 12,
          visible: false
        },
        {
          targets: 13,
          visible: false
        },
        {
          targets: 14,
          visible: false
        },
        {
          targets: 15,
          visible: false
        },
        {
          targets: 16,
          visible: false
        }
      ],
    });


    $('#save').on('click', function () {
      var form_data = $('#addForm').serialize();
      $("#save").prop('disabled', true);
      $('#save').html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Save');
      $.ajax({
        url: APP_URL + "/landholding/newapplicants/",
        type: "POST",
        dataType: "json",
        data: form_data,
        success: function (result) {
          $.toaster({
            message: result.message,
            title: 'Message',
            priority: result.status,
            settings: { timeout: 6000 },
          });
          $("#save").prop('disabled', false);
	        $('#save').html('Save');
          $('#addForm')[0].reset();
          $('#addFormModal').modal('toggle');
          table.ajax.reload();
        }
      });
    });

    $('#example tbody').on('click', 'tr', function () {
      var row_data = table.row(this).data();
      // alert(row_data.reason_for_deferring);
      // var con=confirm("Are you sure you want to delet this ")
      $('#id').val(row_data.applicant_id);
      $('#Ename').val(row_data.applicant_name);
      $('#addr').val(row_data.addr_house_num + '/' + row_data.addr_house_name + ',' + row_data.addr_house_location + ',' + row_data.pincode);
      $('#Eration').val(row_data.ration_number);
      $('#Eadhaar').val(row_data.aadhaar);
      $('#Egender').val(row_data.gender);
      $('#Ecategory').val(row_data.category_id);
      $('#editauthenticationdate').val(row_data.authentication_date);
      if (row_data.minority == 1)
        $('#Eminority').prop("checked", true);
      else
        $("#Eminority").prop("checked", false);   //put else condition coz,unchecked not get activated when value it is 0
      if (row_data.handicapped == 1)
        $('#Ehandicapped').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
      else
        $("#Ehandicapped").prop("checked", false);
      if (row_data.women_lead == 1)
        $('#Ewomenlead').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
      else
        $("#Ewomenlead").prop("checked", false);
      if (row_data.fisherman == 1)
        $('#Efisherman').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
      else
        $("#Efisherman").prop("checked", false);
      if (row_data.plantation == 1)
        $('#Eplantation').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
      else
        $("#Eplantation").prop("checked", false);
      if (row_data.st_remote == 1)
        $('#Est_remote').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
      else
        $("#Est_remote").prop("checked", false);

      var placeofstay = row_data.place_of_stay_id;
      switch (placeofstay) {
        case '1':
          $('#Eplaceofstay1').prop("checked", true);
          break;
        case '2':
          $('#Eplaceofstay2').prop("checked", true);
          break;
        case '3':
          $('#Eplaceofstay3').prop("checked", true);
          break;
        case '4':
          $('#Eplaceofstay4').prop("checked", true);
          break;
        case '5':
          $('#Eplaceofstay5').prop("checked", true);
          break;
        case '6':
          $('#Eplaceofstay6').prop("checked", true);
          break;
        default:
          $('#Eplaceofstay1').prop("checked", false);
          $('#Eplaceofstay2').prop("checked", false);
          $('#Eplaceofstay3').prop("checked", false);
          $('#Eplaceofstay4').prop("checked", false);
          $('#Eplaceofstay5').prop("checked", false);
          $('#Eplaceofstay6').prop("checked", false);
      }



      var placeofstay = row_data.reason_for_deferring;
      switch (placeofstay) {
        case '1':
          $('#Ereason1').prop("checked", true);
          break;
        case '2':
          $('#Ereason2').prop("checked", true);
          break;
        case '3':
          $('#Ereason3').prop("checked", true);
          break;
        case '4':
          $('#Ereason4').prop("checked", true);
          break;
        default:
          $('#Ereason1').prop("checked", false);
          $('#Ereason2').prop("checked", false);
          $('#Ereason3').prop("checked", false);
          $('#Ereason4').prop("checked", false);
          break;
      }

      $('#editFormModal').modal('toggle');
    });

    $('#edit').on('click', function () {
      var form_data = $('#editForm').serialize();
      var id = $('#id').val();
      $("#edit").prop('disabled', true);
      $('#edit').html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Save');
      $.ajax({
        url: APP_URL + "/landholding/newapplicants/" + id,
        type: "PUT",
        dataType: "json",
        data: form_data,
        success: function (result) {
          $("#edit").prop('disabled', false);
	        $('#edit').html('Save');
          $.toaster({
            message: result.message,
            title: 'Message',
            priority: result.status,
            settings: { timeout: 6000 },
          });
          $('#editForm')[0].reset();
          $('#editFormModal').modal('toggle');
          table.ajax.reload();
        }
      });
    });



    $('#delete').on('click', function () {
      var form_data = $('#addForm').serialize();
      var id = $('#id').val();
      var con = confirm("Are you sure you want to delete this ")
      if (con == true) {
        $.ajax({
          url: APP_URL + "/landholding/newapplicants/" + id,
          type: "DELETE",
          dataType: "json",
          data: form_data,
          success: function (result) {
            $.toaster({
              message: result.message,
              title: 'Message',
              priority: result.status,
              settings: { timeout: 6000 },
            });
            $('#addForm')[0].reset();
            $('#editFormModal').modal('toggle');
            table.ajax.reload();
          }
        });
      }
    });




  });
  function isNumber(event) {
    var inputValue = event.which;
    // allow numbers and null (0), backspace (8), del(127) .
    if (!(inputValue > 47 && inputValue < 58) && (inputValue != 0 && inputValue != 8 && inputValue != 127)) {
      event.preventDefault();
    }
  }


</script> @endpush @endsection
