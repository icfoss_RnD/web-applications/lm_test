@extends('layouts.app')
@section('title', 'Beneficiaries')

@section('breadcrumbs')
<li >/ <a href="#">Validation</a> </li>
@endsection



@section('content')

@if($signature_status == 0 or $signature_status == '')
<div class="container-fluid">
  <div class="col-md-12">
    <h2 class="meera">ഗുണഭോക്തൃ വിശദാംശ പരിശോധന</h2>
    <div class="alert alert-info alert-dismissible meera" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>നിർദേശങ്ങൾ :</strong> 
      താങ്കളുടെ തദ്ദേശ സ്വയം ഭരണ സ്ഥാപനത്തിലെ ഗുണഭോക്താക്കളുടെ, നിലവിൽ കമ്പ്യൂട്ടറൈസ് ചെയ്തിട്ടുള്ള വിവരങ്ങളാണ് ഈ പട്ടികയിൽ നൽകിയിട്ടുള്ളത്. 
      ഇതിലെ എല്ലാ വിവരങ്ങളും പരിശോധിച്ച് ഉറപ്പാക്കേണ്ടതാണ്. റേഷൻകാർ‍ഡ് നമ്പ‍ർ, ആധാ‍ർ നമ്പർ, ഗുണഭോക്താവ് ഉൾപ്പെടുന്ന 
      വിഭാഗം എന്നിവയിൽ തിരുത്തു വരുത്തുന്നതും ഗുണഭോക്താവ് ഉൾപ്പെടുന്ന വിഭാഗവും ഉപവിഭാഗവും രേഖപ്പെടുത്തുന്നതും സെക്രട്ടറി അഥവാ സെക്രട്ടറി ചുമതലപ്പെടുത്തിയയാൾ പരിശോധിച്ചു ബോധ്യപ്പെട്ട വിവരങ്ങൾ അടിസ്ഥാനമാക്കിയാകണം
    </div>
  </div>
  <table id="example" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
    <thead>
      <tr>
        <th>Beneficiary Id</th>
        <th>Name of Beneficiary</th>
        <th>Address</th>
        <th>Ration Card Number</th>
        <th>Aadhaar Number</th>
        <th>Gender</th>
        <th>Category</th>
      </tr>
    </thead>
  </table>
</div>
@else
<p class="unsigned-alert">You have completed verification process.</p>
@endif
<div id="addFormModal" class="modal fade meera" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="height:500px;overflow-y: auto;">
      <div class="modal-header">
        <h3 class="meera">വിശദാംശ പരിശോധനാ ഫോം -ഘട്ടം 1</h3>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container" style="width:100%">
          <div class="row">
            <form id="addForm" class="form form-horizontal" method="post">
              {{ csrf_field() }}
              <input type="hidden" id="id" name="id" />
              <div class="form-group">
                <label class="col-sm-12 meera" for="name">ഗുണഭോക്താവിന്റെ പേര്:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="name" name="name"   disabled/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr">ഗുണഭോക്താവിന്റെ മേല്‍ വിലാസം:</label>
                <div class="col-sm-12">
                  <input type="addr" class="form-control" id="addr" name="addr"  disabled />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="ration">റേഷന്‍ കാര്‍ഡ് നമ്പര്‍:
                  <span class="meera" style="color:red; font-size:12px;" > *ഒത്തുനോക്കി ഉറപ്പ് വരുത്തുക</span>
                </label>
                <div class="col-sm-12">
                  <input onkeypress="isNumber(event)" maxlength="10" type="text" class="form-control" id="ration" name="ration" />
                </div>
              </div>
	      <div class="form-group">
                <label class="col-sm-12 meera" for="adhaar">ആധാര്‍ കാര്‍ഡ് നമ്പര്‍:
                  <span class="meera" style="color:red; font-size:12px" > *ഒത്തുനോക്കി ഉറപ്പ് വരുത്തുക</span>
                </label>
                <div class="col-sm-12">
                  <input onkeypress="isNumber(event)" maxlength="12" type="text" class="form-control" id="adhaar" name="adhaar" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="gender">ലിംഗ പദവി:</label>
                <div class="col-sm-12">
                  <select id="gender" name="gender" class="form-control">
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                    <option value="T">Transgender</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-12 meera" for="category">വിഭാഗം:</label>
                <div class="col-sm-12">
                  <select id="category" name="category" class="form-control meera">
                    <option value="1">പൊതുവിഭാഗം</option>
                    <option value="2">പട്ടികവര്‍ഗ്ഗം </option>
                    <option value="3">പട്ടികജാതി </option>
                    <option value="4">ഒ.ബി.സി </option>
                    </select>
                </div>
              </div>
              <div class="form-group subcategory">
              <input type="checkbox" class="" id="minority" name="minority" />
                <label class="meera" for="minority">മൈനോറിറ്റി </label>
               <input type="checkbox" class="" id="handicapped" name="handicapped" />
                <label class="meera" for="handicapped">അംഗപരിമിതർ</label>
              <input type="checkbox" class="" id="womenlead" name="womenlead" />
                <label class="meera" for="womenlead">സ്ത്രീ കേന്ദ്രീത കുടുംബം</label><br>
              <input type="checkbox" class="" id="fisherman" name="fisherman" />
                <label class="meera" for="fisherman">മത്സ്യ ബന്ധനത്തൊഴിലാളി</label>
              <input type="checkbox" class="" id="plantation" name="plantation" />
                <label class="meera" for="plantation">പ്ലാന്റേഷന്‍ തൊഴിലാളി</label></br>
              <input type="checkbox" class="" id="st_remote" name="st_remote" />
                <label class="meera" for="st_remote">വിദൂര സങ്കേതങ്ങളിലെ പട്ടികവര്‍ഗ്ഗം </label>
                </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="resolutiondate">മേൽ വ്യക്തി ഉള്‍പ്പെട്ട ഗുണഭോക്തൃപ്പട്ടിക ത.സ്വ.ഭ സ്ഥാപന ഭരണ സമിതി അംഗീകരിച്ച തീയതി:</label>
                <div class="col-sm-12">
                  <input type="text" autocomplete="on" class="form-control dt-picker" id="resolutiondate" name="resolutiondate" required/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="resolutionnum">ത.സ്വ.ഭ സ്ഥാപന ഭരണ സമിതി അംഗീകാര നമ്പര്‍:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="resolutionnum" name="resolutionnum" required/>
                </div>
              </div>
              <!-- <div class="form-group">
                <div class="col-sm-12" style="display: inline;padding-right: 0;">
                  <input type="checkbox" class="" id="possession" name="possession" />
                </div>
                <label style="display: inline;margin: 0;padding: 3px;" class="col-sm-12 meera" for="possession">ഭൂമിയുടെ കെെവശാവകാശരേഖ / ആധാരം / പട്ടയം നേരിട്ടു പരിശോധിച്ചു ബോധ്യപ്പെട്ടു:</label>
              </div> -->
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- .modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
} -->



<div id="Intromodal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->

    <div class="modal-content meera">

      <h3 class="meera">തദ്ദേശ സ്വയംഭരണ സ്ഥാപന സെക്രട്ടറിയ്ക്കുള്ള നിര്‍ദ്ദേശങ്ങള്‍</h3>
      <p>തദ്ദേശ സ്വയംഭരണ സ്ഥാപന ഭരണസമിതി ലെെഫ് ഗുണഭോക്തൃപ്പട്ടികയ്ക്ക് അംഗീകാരം നല്‍കിയ മിനിറ്റ്സിന്റെ അടിസ്ഥാനത്തിലും അതോടൊപ്പമുള്ള അംഗീകൃത ഗുണഭോക്തൃപ്പട്ടികയുമായി ഒത്തുനോക്കിയുമാണ് ഇവിടെ നല്‍കിയിട്ടുള്ള പട്ടിക അംഗീകരിക്കേണ്ടത്.
      സപ്ലിമെന്ററി ഗുണഭോക്തൃപ്പട്ടികയിലുള്ളവരുടെ പരിശോധന ഇപ്പോള്‍ ഇതിലുള്‍പ്പെടുന്നില്ല.
      </p>
      <p>ലെെഫ് ഗുണഭോക്തൃ സര്‍വ്വേ വഴിയോ തദ്ദേശ സ്വയംഭരണ സ്ഥാപനതലത്തിലും ജില്ലാ കളക്ടറുടെ തലത്തിലുമുള്ള അപ്പീലുകള്‍ വഴിയോ കണ്ടെത്തുകയും ഗ്രാമസഭ അംഗീകരിക്കുകയും ചെയ്ത മുഖ്യഗുണഭോക്തൃപ്പട്ടികയിലുള്ളവരില്‍ ഏതെങ്കിലും ഗുണഭോക്താക്കളുടെ പേര് തദ്ദേശ
      സ്വയംഭരണ സ്ഥാപന ഭരണസമിതിയുടെ അംഗീകാര പട്ടികയില്‍ ഉള്‍പ്പെടാതെ പോയിട്ടുണ്ടെങ്കില്‍ അതുവിട്ടുപോകാനുണ്ടായ കാരണം പ്രത്യേകം കാണിച്ചു കൊണ്ട് പ്രസ്തുത തദ്ദേശ സ്വയംഭരണ സ്ഥാപന ഭരണസമിതി തന്നെ തീരുമാനമെടുത്ത് പരിഹാരമുണ്ടാക്കേണ്ടതാണ്.
      തദ്ദേശ സ്വയംഭരണ സ്ഥാപന ഭരണസമിതി അപ്രകാരം നൽകിയ ഗുണഭോക്താക്കളുടെ പട്ടിക <br> അപ്​ലോഡ് ചെയ്യാനുള്ള ഫോറം നിലവിലുള്ള ഗുണഭോക്താക്കളുടെ പട്ടിക  സാക്ഷ്യപ്പെടുത്തി അന്തിമമാക്കുന്ന മുറയ്ക്ക് ഈ സൈറ്റിൽ ലഭ്യമാക്കുന്നതാണ്.
      </p>
      <p>തദ്ദേശ സ്വയംഭരണ സ്ഥാപന ഭരണസമിതി അംഗീകരിച്ച ഗുണഭോക്താക്കള്‍ക്ക് ഭവന നിര്‍മ്മാണ ധനസഹായം വിതരണം ചെയ്യുന്നതിന് മാര്‍ഗ്ഗ നിര്‍ദ്ദേശ ഉത്തരവ് അനുശാസിക്കുന്നതുപോലെ താഴെപ്പറയുന്ന രേഖകള്‍ നിര്‍വ്വഹണ ഉദ്യോഗസ്ഥര്‍ നേരിട്ടു പരിശോധിച്ചിരിക്കേണ്ടതും ആയവയുടെ പകര്‍പ്പുകള്‍ നിര്‍വ്വഹണ ഉദ്യോഗസ്ഥരുടെ പക്കല്‍ ലഭ്യമാക്കിയിരിക്കേണ്ടതുമാണ്.</p>
      <p>
        <ol>
          <li>റേഷന്‍ കാര്‍ഡ് (അതില്‍ ഗുണഭോക്താവിന്റെ പേരുണ്ടായിരിക്കണം. കാര്‍ഡില്‍ പേരുള്ള അംഗങ്ങളില്‍ ആര്‍ക്കും സ്വന്തമായി വീടുണ്ടായിരിക്കാന്‍ പാടില്ല). </li>
          <li>ഗുണഭോക്താവിന്റെ ആധാര്‍ കാര്‍ഡ്.</li>
          <li>വീടുവയ്ക്കാനുദ്ദേശിക്കുന്ന ഭൂമിയുടെ കെെവശാവകാശ രേഖ വില്ലേജ് ഓഫീസില്‍ നിന്നും ലഭ്യമാക്കിയത്. ( ഭൂമിയുടെ കെെവശാവകാശം ഗുണഭോക്താവിന്റെ പേരില്‍ അല്ലെങ്കില്‍ കെെവശാവകാശമുള്ളയാള്‍, അവിടെ വീടു് വച്ച് 12 വര്‍ഷത്തില്‍ കുറയാത്തകാലം താമസിക്കുന്നതിന് ഗുണഭോക്താവിന് നിയമപരമായി നല്‍കുന്ന സമ്മതപത്രം കൂടി ലഭ്യമാക്കണം.)</li>
          <li>ബാങ്ക് അക്കൗണ്ട് പാസ്സ് ബുക്ക്.</li>
          <li>ഗുണഭോക്താവ് തദ്ദേശ ഭരണ സ്ഥാപനത്തിനു നല്‍കുന്ന സമ്മതപത്രം.</li>
          <li>തദ്ദേശ ഭരണ സ്ഥാപനം നല്‍കുന്ന ബില്‍ഡിങ് പെര്‍മിറ്റ്.</li>
        </ol>
    <button type="button" class="close col-sm-2 btn btn-primary "style="margin:auto; padding:5px;" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
@push('bodyscripts')
<script>

$(document).ready(function(){
    $('#resolutiondate').datetimepicker({
        lang:'en',
        timepicker:false,
        minDate:'2017/01/01',
        maxDate: '0',
        format:'Y-m-d',
        allowBlank: true,
        scrollInput: false
    });

  $('#Intromodal').modal('toggle');
    var table = $('#example').DataTable({
        "processing": true,
        "serverSide": true,
        "searching": true,
        "ajax":
        {
        "url": APP_URL+"/landholding/beneficiaries",
        "type": "GET",
        "dataType":"json"
        },
        columns: [
            {data: 'beneficiary_id', name: 'beneficiary_id'},
            {data: 'beneficiary_name', name: 'beneficiary_name'},
            {data: 'addr_house_num', name: 'addr_house_num'},
            {data: 'ration_number', name: 'ration_number'},
            {data: 'aadhaar', name: 'aadhaar'},
            {data: 'gender', name: 'gender'},
            {data: 'category_name', name: 'category_name'},
            {data: 'possession_verified', name: 'possession_verified'},
            {data: 'resolution_date', name: 'resolution_date'},
            {data: 'resolution_num', name: 'resolution_num'},

            {data: 'minority', name: 'minority'},
            {data: 'handicapped', name: 'handicapped'},
            {data: 'women_lead', name: 'women_lead'},
            {data: 'fisherman', name: 'fisherman'},
            {data: 'plantation', name: 'plantation'},
            {data: 'st_remote', name: 'st_remote'},

        ],
        columnDefs: [
            {
              render: function(data, type ,row, meta){
                return data + '/\n' +row.addr_house_name + ',\n' +row.addr_house_location +',\n'+row. pincode
              },
              targets: 2
            },
        {
            targets: 7,
            visible: false
        },
        {
            targets: 8,
            visible: false
        },
        {
            targets: 9,
            visible: false
        },
        {
            targets: 10,
            visible: false
        },
        {
            targets: 11,
            visible: false
        },
        {
            targets: 12,
            visible: false
        },
        {
            targets: 13,
            visible: false
        },
        {
            targets: 14,
            visible: false
        },
        {
            targets: 15,
            visible: false
        }
        ],
        "fnRowCallback" : function(nRow, aData, iDisplayIndex)
        {
          // colouring data_rows with resolution_date and resolution_num
          if(aData.resolution_date != null && aData.resolution_num != null){
            $(nRow).addClass('green');
          }
          return nRow;
        },
    });

    $('#save').on('click',function(){
        var id = $('#id').val();

        var form_data = $('#addForm').serialize();
        $("#save").prop('disabled', true);
      $('#save').html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Save');
        $.ajax({
            url: APP_URL+"/landholding/beneficiaries/"+id,
            type: "PUT",
            dataType: "json",
            data: form_data,
            success: function(result){
              $("#save").prop('disabled', false);
	            $('#save').html('Save');
                $.toaster({
                    message : result.message,
                    title : 'Message',
                    priority : result.status,
                    settings: { timeout : 6000 },
                });
                $('#addForm')[0].reset();
                $('#addFormModal').modal('toggle');
                $("#possession").prop("checked", false);
                table.ajax.reload();
            }
        });
    });

    $('#example tbody').on( 'click', 'tr', function () {
        var row_data = table.row(this).data();
        //console.log(row_data.st_remote);
        $('#id').val(row_data.beneficiary_id);
        $('#name').val(row_data.beneficiary_name);
        $('#addr').val(row_data.addr_house_num + '/' +row_data.addr_house_name + ',' +row_data.addr_house_location +','+row_data. pincode);
        $('#ration').val(row_data.ration_number);
        $('#adhaar').val(row_data.aadhaar);
        $('#gender').val(row_data.gender);
        $('#category').val(row_data.category_id);

        if(row_data.possession_verified==1)
          $('#possession').prop("checked", true);
        else
          $("#possession").prop("checked", false);
        $('#resolutionnum').val(row_data.resolution_num);
        $('#resolutiondate').val(row_data.resolution_date);
        if(row_data.minority==1)
            $('#minority').prop("checked", true);
        else
            $("#minority").prop("checked", false);   //put else condition coz,unchecked not get activated when value it is 0
        if(row_data.handicapped==1)
            $('#handicapped').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
        else
            $("#handicapped").prop("checked", false);
        if(row_data.women_lead==1)
            $('#womenlead').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
        else
            $("#womenlead").prop("checked", false);
        if(row_data.fisherman==1)
            $('#fisherman').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
        else
            $("#fisherman").prop("checked", false);
        if(row_data.plantation==1)
            $('#plantation').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
        else
            $("#plantation").prop("checked", false);
        if(row_data.st_remote==1)
            $('#st_remote').prop("checked", true); //put else condition coz,unchecked not get activated when value it is 0
        else
            $("#st_remote").prop("checked", false);
        $('#addFormModal').modal('toggle');
    });
});

// number validation

function isNumber(event) {
  var inputValue = event.which;
  // allow numbers and null (0), backspace (8), del(127) .
  if(!(inputValue > 47 && inputValue < 58) && (inputValue != 0 && inputValue != 8 && inputValue != 127)) {
      event.preventDefault();
  }
}
</script>
@endpush
@endsection
