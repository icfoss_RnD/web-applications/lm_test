@extends('layouts.app') 
@section('title', 'Migrations')
 @section('breadcrumbs')
<li>/ <a href="#">Local Body Migration </a>
    / <a href="#">Initiate Request</a>
</li>
@endsection
@section('content')

      <div class="container" style="width:100%">
      <h3 class="meera"> Initiate Request</h3>
          <div class="row">
            <form id="addForm" class="form form-horizontal" method="post" >
              {{ csrf_field() }}
              <input type="hidden" id="id" name="id" />
              <div class="form-group">
                <label class="col-sm-6 meera" for="benid">Beneficiary Id:</label>
                <div class="col-sm-6">
                  <input onkeypress="isNumber(event)" maxlength="10" type="text" class="form-control" id="benid" name="benid"  required  />
                </div>
             
              <label class="col-sm-6 meera" for="addr">Local Body Name</label>
                <div class="col-sm-6">
              <select class="form-control" name="lbname" id="lbname" data-parsley-required="true">
              <option value="" selected="selected">-----------------------------------Select A LocalBody----------------------------</option>
              @foreach ($local_bodies as $lb) 
            <option value="{{ $lb->local_body_id }}">{{ $lb->local_body_display_name }}</option>
            @endforeach
            </select>
                 </div>
              </div>
                <button class="btn btn-primary" type="button" id="transfer">Transfer</button>
              </form>
            </div>

    <!-- confirmTransfer Modal -->

    <div id="confirmTransfer" class="modal fade meera" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="height:500px;overflow-y: auto;">
      <div class="modal-header">
        <h3 class="meera">Initiate Transfer Request</h3>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container" style="width:100%">
          <div class="row">
            <form id="addForm" class="form form-horizontal" >
              {{ csrf_field() }}
              <input type="hidden" id="id" name="id" />
              <div class="form-group">
                <label class="col-sm-12 meera" for="name">ഗുണഭോക്താവിന്റെ പേര്:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="name" name="name"   disabled/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="addr">ഗുണഭോക്താവിന്റെ മേല്‍ വിലാസം:</label>
                <div class="col-sm-12">
                  <input type="addr" class="form-control" id="addr" name="addr"  disabled />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 meera" for="ration">റേഷന്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input  type="text" class="form-control" id="ration" name="ration" disabled/>
                </div>
              </div>
	         <div class="form-group">
                <label class="col-sm-12 meera" for="adhaar">ആധാര്‍ കാര്‍ഡ് നമ്പര്‍:
                </label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="adhaar" name="adhaar" disabled/>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="transferRequest">Create Transfer Request</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
                </form>
              </div>
            </div>
        </div>
        </div>
        </div>
        </div>
        </div>
    <!-- confirmTransfer Modal Ends-->


            <br>
            <div class="row">
            <h3 class="meera">Local Body Migration Requests </h3>
            </div>
             <table id="example" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
            <thead>
                <tr>
                    <th>Beneficiary Id</th>
                    <th>Beneficiary Name</th>
                    <th>Local Body Name</th>
                    <th>Migration Status</th>
                    <th>Submission Date</th>
                    <th>Approval Date</th>
                    
                </tr>
            </thead>
        </table>
    </div>
    </div>
    @push('bodyscripts')
<script>

$(document).ready(function(){
    var table = $('#example').DataTable({
        "processing": true,
        "serverSide": true,
        "searching": true,
        "ajax":
        {
        "url": APP_URL+"/landholding/initiatetransferrequest",
        "type": "GET",
        "dataType":"json"
        },
        columns: [
            {data: 'beneficiary_id', name: 'beneficiary_id'},
            {data: 'beneficiary_name', name: 'beneficiary_name'},
            {data: 'local_body_display_name', name: 'local_body_display_name'},
            {data: 'status_flag', name: 'status_flag'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'},
        ],
        columnDefs: [
            {
            targets : 3,
            render : function(data, type ,row, meta){
              if(data == '1')
                return 'Approved';
              else
                return 'Pending';
            },
            },
        ],
    });
 

  $('#transfer').on('click',function(){
         var benid = $('#benid').val()
        // alert(benid);
	$("#transfer").prop('disabled', true);
        $('#transfer').html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Transfer');
        $.ajax({
            url: APP_URL+"/landholding/initiatetransferrequest/"+benid,
            type: "GET",
            dataType: "json",
                success: function(result){
                    if(result.status=='warning'){
                    console.log(result.beneficiaryData);
                    $.toaster({
                    message : result.message,
                    title : 'Message',
                    priority : result.status,
                    settings: { timeout : 6000 },
                });
                    } else {
                        console.log(result.beneficiaryData[0].beneficiary_name);
                        $('#name').val(result.beneficiaryData[0].beneficiary_name);
                        $('#addr').val(result.beneficiaryData[0].addr_house_num +','+ result.beneficiaryData[0].addr_house_name +','+ result.beneficiaryData[0].addr_house_location +','+ result.beneficiaryData[0].pincode);
                        $('#ration').val(result.beneficiaryData[0].ration_number);
                        $('#adhaar').val(result.beneficiaryData[0].aadhaar);
                    $("#confirmTransfer").modal('show');
                    }
              	$("#transfer").prop('disabled', false);
        	$('#transfer').html('Transfer');
            }
        });
  });

$('#lbname').select2();

    $('#transferRequest').on('click',function(){
         var benid = $('#benid').val()
         var lb_id = $('#lbname').val()
         console.log(lb_id);
        $("#transferRequest").prop('disabled', true);
        $('#transferRequest').html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Create Transfer Request');
        $.ajax({
            url: APP_URL+"/landholding/initiatetransferrequest/"+benid,
            type: "PUT",
            dataType: "json",
            data: {lb_id:lb_id},
            headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
                success: function(result){
                  $.toaster({
                    message : result.message,
                    title : 'Message',
                    priority : result.status,
                    settings: { timeout : 6000 },
                }); 
		$("#transferRequest").prop('disabled', false);
        	$('#transferRequest').html('Create Transfer Request');
                $('#confirmTransfer').modal('toggle');
                $('#addForm')[0].reset();
                $('#lbname').val('').trigger('change');
                
                table.ajax.reload(); 
               
            }
        });
  });
});
  function isNumber(event) {
  var inputValue = event.which;
  // allow numbers and null (0), backspace (8), del(127) .
  if(!(inputValue > 47 && inputValue < 58) && (inputValue != 0 && inputValue != 8 && inputValue != 127)) {
      event.preventDefault();
  }
}
</script>
        @endpush
        @endsection
        
