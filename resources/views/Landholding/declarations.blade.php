@extends('layouts.app')
@section('title', 'Beneficiaries')

@section('breadcrumbs')
<li >/ <a href="#">Declaration</a> </li>
@endsection

@section('content')
<div class="container col-lg-12" style="width:100%">
        <div class="row">
        <div class="col-md-12">
          <h2 class="meera">സാക്ഷ്യപ്രസ്താവന</h2>
    
    <div class="alert alert-danger alert-dismissible meera"  role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>നിർദേശങ്ങൾ :</strong> എല്ലാ ഗുണഭോക്താക്കളുടെയും വിശദാംശ പരിശോധന പൂർത്തിയാക്കിയതിന് ശേഷം മാത്രം ഡിജിറ്റൽ സിഗ്നേച്ചർ ഉപയോഗിച്ച് സാക്ഷ്യപ്രസ്താവന നൽകുക. പ്രത്യേകം ശ്രദ്ധിക്കുക, ഈ ഘട്ടത്തിന് ശേഷം ഗുണഭോക്തൃപ്പട്ടികയിൽ മാറ്റം വരുത്താനൊ കൂട്ടിചേർക്കാനൊ കഴിയില്ല.
      താങ്കളുടെ ത.സ്വ.ഭ സ്ഥാപനത്തിലെ ഗുണഭോക്തൃപട്ടികയിൽ ആകെയുണ്ടായിരുന്ന<u style="color:black;"> <strong>{{$beneficiarycount}}</strong> പേരിൽ നിന്നും <strong><span id="validatedcount"> </span> </strong></u>പേർക്കാണ് ഇപ്പോൾ താങ്കൾ അംഗീകാരമുണ്ടെന്ന് കാണിച്ചിട്ടുള്ളത്. ഒപ്പു വയ്ക്കുന്നതോടെ ഗുണഭോക്തൃപട്ടിക അംഗീകാരമുള്ളവർ മാത്രമായി ചുരുങ്ങും. പിന്നീട് അവർക്കു മാത്രമേ ധനസഹായം നൽകാനാകൂ എന്നതു ശ്രദ്ധിക്കുക
    </div>

        </div>
        @if($signature_status == 0)
        <form id="addForm" class="form form-horizontal" method="post" action="{{action('Landholding\DeclarationController@store')}}" >
        {{ csrf_field() }}
        <input type="hidden" id="lb_id" name="lb_id" value="{{$lbdetails[0]->local_body_id}}" />
            <div class="form-group">
                <label class="col-sm-6 meera" for="name">തദ്ദേശ സ്വയംഭരണ സ്ഥാപനത്തിന്റെ പേര് :</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="name" name="name" value="{{ $lbdetails[0]->local_body_display_name }}" disabled />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-6 meera" for="count">അംഗീകൃത, ഭൂമിയുള്ള ഭവനരഹിത ഗുണഭോക്താക്കളുടെ ആകെ എണ്ണം: </label>
		<span id="count_label"></span>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="count_dummy" name="count_dummy" disabled />
                    <input type="hidden" class="form-control" id="count" name="count" />
                </div>
            </div>
            <!-- <div class="form-group">
                <label class="col-sm-6" for="comments">Comments:</label>
                <div class="col-sm-6"> -->
                    <input type="hidden" class="form-control" id="comments" name="comments" />
<!--                </div>
            </div> -->
            <div class="form-group">
                <div class="col-sm-6" style="display: inline;padding-right: 0;">
                    <input type="checkbox" class="" id="sign" name="sign" />
                </div>
                <label style="display: inline;margin: 0;padding: 3px;" class="col-sm-6 meera" for="sign">ഈ ത.സ്വ.ഭ സ്ഥാപനത്തിലെ, ഭൂമിയുള്ള ഭവനരഹിത  ഗുണഭോക്താക്കളുടെ നിലവിലുള്ള അംഗീകൃത പട്ടിക താഴെക്കൊടുക്കുന്നു. പട്ടികയിലെ  ഗുണഭോക്താക്കളെ  ഈ ത.സ്വ.ഭ സ്ഥാപനത്തിലെ വിവിധ ഗ്രാമസഭകളും ഭരണസമിതിയും അംഗീകരിച്ചിട്ടുള്ളതാണ്. ഇവരുടെ വിവരങ്ങള്‍ പരിശോധിച്ച് ഉറപ്പാക്കിയിട്ടുണ്ട്.
                
                </label>
            </div>

               

            <div id="signAppletDiv" class="form-group" style="display:none">
            <div class="col-sm-6 col-sm-offset-4">
                <applet id="signApp" width=200 height=100
                type="application/x-java-applet;jpi-version=6"
                archive="{{URL::asset('/applet/digisign_final7.jar')}}"
                code="digitalsignlifemission/DigitalSignlife.class">
                Applet failed to load!
                </applet>
            </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-4">
                <span style="color: #AAAAAA;">Signature:</span> <input type="text" class="form-control" id="signedData" name="signedData" readonly/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-4">
                    <button type="submit" class="btn btn-primary" id="save">Submit</button>
                </div>
            </div>
        </form>
        @else
        <div class="col-md-12">
            <p class="unsigned-alert">You have already completed beneficiary verification and declaration process.</p>
        </div>
        @endif
        </div>
        </div>
<div class="col-md-12">
<table id="example" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
        <thead>
            <tr>
            <th>Beneficiary Id</th>
                <th>Name of Beneficiary</th>
                <th>Address</th>
                <th>Ration Card Number</th>
                <th>Aadhaar Number</th>
                <th>Gender</th>
                <th>Category</th>
            </tr>
        </thead>
    </table>
</div>
<!-- declaration container -->





@push('bodyscripts')
<script>
function setSignature(param1){
  if(param1!='')
    $('#signedData').val(param1);
  else {
    alert("signing failed!");
  }
}

$(document).ready(function(){
    var table = $('#example').DataTable({
        "processing": true,
        "serverSide": true,
        "searching": true,
        "ajax":
        {
        "url": APP_URL+"/landholding/declarations",
        "type": "GET",
        "dataType":"json",
        },
        columns: [
            {data: 'beneficiary_id', name: 'beneficiary_id'},
            {data: 'beneficiary_name', name: 'beneficiary_name'},
            {data: 'addr_house_num', name: 'addr_house_num'},
            {data: 'ration_number', name: 'ration_number'},
            {data: 'aadhaar', name: 'aadhaar'},
            {data: 'gender', name: 'gender'},
            {data: 'category_name', name: 'category_name'},
        ],
        columnDefs: [
            {
            render: function(data, type ,row){
                return data + '/\n' +row.addr_house_name + ',\n' +row.addr_house_location +',\n'+row.pincode
            },
            targets: 2
        }
        ],
	    "fnRowCallback" : function(nRow, aData, iDisplayIndex)
        {
          countData();
          return nRow;
        },
    });

    function countData(){
        $('#count_dummy').val(table.page.info().recordsTotal);
        $('#count').val(table.page.info().recordsTotal);
        $('#validatedcount').text(table.page.info().recordsTotal);
    }

    $('#sign').on('click',function(){
        if($(this).is(":checked")) {
            var app = document.getElementById('signApp');
            var p1 = document.createElement('param');
            var p2 = document.createElement('param');
            var p3 = document.createElement('param');
            p1.name = 'lb_id';
            p2.name = 'count';
            p3.name = 'comments';
            p1.value=$('#lb_id').val();
            p2.value=$('#count').val();
            p3.value=$('#comments').val();
            app.appendChild(p1);
            app.appendChild(p2);
            app.appendChild(p3);
            $("#signAppletDiv").show();
        }
        else{
            $("#signAppletDiv").hide();
        }
    });
});
</script>
@endpush
@endsection
