@extends('layouts.app')
@section('title', 'Beneficiaries')

@section('breadcrumbs')
<li >/ <a href="#">Fund Declaration</a> </li>
@endsection

@section('content')
@if($signature_status == 0 or $signature_status == '')
<div>
    <p class="unsigned-alert">You have not completed beneficiary verification and declaration process.</p>
</div>
@else
<h2 class="meera">ധനവിനിയോഗ വിശദാംശം</h2>
<div class="col-md-12">
<!-- declaration container -->
<div class="container" style="width:100%">
    <div class="row">
    <form id="addForm" class="form form-horizontal" method="post" autocomplete="off">
    {{ csrf_field() }}
    <div class="form-group">
        <label class="col-sm-4 meera" for="name">തദ്ദേശ സ്വയംഭരണ സ്ഥാപനത്തിന്റെ പേര്:</label>
        <label class="" for="name">{!! $lbdetails[0]->local_body_display_name !!}</label>
    </div>
    <div class="col-md-12">
    <table id="example" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
        <thead>
            <tr>
            <th>Fund Disbursion</th>
                <th>Stage 1</th>
                <th>Stage 2</th>
                <th>Stage 3</th>
                <th>Stage 4</th>
                <th>Stage 5</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody class="meera">
            <tr id="fundpending">
                <td>ഉടനെ വിതരണം ചെയ്യേണ്ടതായ ധനസഹായം</td>
                <td title="ST_REMOTE: {{ $funddata['stage1'][0] }}, OTHERS: {{ $funddata['stage1'][1] }}">{{ $funddata['stage1'][0]*90000+$funddata['stage1'][1]*40000 }}</td>
                <td title="ST_REMOTE: {{ $funddata['stage2'][0] }}, OTHERS: {{ $funddata['stage2'][1] }}">{{ $funddata['stage2'][0]*120000+$funddata['stage2'][1]*160000 }}</td>
                <td title="ST_REMOTE: {{ $funddata['stage3'][0] }}, OTHERS: {{ $funddata['stage3'][1] }}">{{ $funddata['stage3'][0]*210000+$funddata['stage3'][1]*160000 }}</td>
                <td title="ST_REMOTE: {{ $funddata['stage4'][0] }}, OTHERS: {{ $funddata['stage4'][1] }}">{{ $funddata['stage4'][0]*120000+$funddata['stage4'][1]*40000 }}</td>
                <td title="ST_REMOTE: {{ $funddata['stage5'][0] }}, OTHERS: {{ $funddata['stage5'][1] }}">{{ $funddata['stage5'][0]*60000 }}</td>
                <td id="total_fund_pending">0</td>
            </tr>
        </tbody>
    </table>
    </div>
    <br/>
    <div class="form-group">
    <label class="col-sm-12 meera" for="yearmarkedfund">തനതു വര്‍ഷം ലെെഫ് ഭവന പദ്ധതിക്കായി അംഗീകരിച്ച പദ്ധതിപ്രകാരം നീക്കി വച്ച ആകെ തുക</label>
        <div class="col-sm-12">
            <input onkeypress="isNumber(event)" type="text" class="form-control" id="yearmarkedfund" name="yearmarkedfund" />
        </div>
    </div>   
    <div class="form-group">
        <label class="col-sm-12 meera" for="pendingfund">ഇപ്പോള്‍ വിതരണം ചെയ്യുന്നതിനായി തസ്വഭ സ്ഥാപനത്തിന്റെ പക്കല്‍ ലഭ്യമായ തുക</label>
        <div class="col-sm-12">
            <input onkeypress="isNumber(event)" type="text" class="form-control" id="fundtobereleased" name="pendingfund" />
        </div>
    </div> 
    <div class="form-group">
        <label class="col-sm-12 meera" for="requestedfund">അധികമായി ലഭ്യമാക്കേണ്ട തുക: </label>
        <div class="col-sm-12">
            <input type="text" class="form-control" id="calc-fund-field" name="requestedfund" readonly />
        </div>
    </div> 
    <!-- <div class="form-group">
        <div class="col-sm-4 col-sm-offset-4">
        <button type="submit" class="btn btn-primary" id="save">Submit</button>
    </div> -->
    </div>
    </form>
    </div>
    <hr/>
    <!-- <div class="row">
    <div class="col-md-12">
    <div style="margin:auto;width:90%">
    <h3>Additional Fund Requests</h3>
    <table id="demands-table" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
    <thead>
        <tr>
            <th>Demand Date</th>
            <th>Fund yearmarked</th>
            <th>Amount in hand</th>
            <th>Fund requested</th>
        </tr>
    </thead>
    </table>
    </div>
    </div> -->
    </div>
</div>

@push('bodyscripts')
<script>
$(document).ready(function(){
//    var sum_released = 0;
    var sum_pending = 0;
    // $('#example tr#fundreleased td').each(function(index) {
    //     if(index!=0 && index!=6){
    //         var data = $(this).text();
    //         sum_released = sum_released + parseInt(data);
    //     }
    // });
    $('#example tr#fundpending td').each(function(index) {
        if(index!=0 && index!=6){
            var data = $(this).text();
            sum_pending = sum_pending + parseInt(data);
        }
    });
    // $('#total_fund_released').val(sum_released);
    // $('#total_fund_released').text(sum_released);
    $('#total_fund_pending').text(sum_pending);
    $('#fundtobereleased').on('keyup',function(){
        calculateFund();
    });

    $('#addForm').on('submit', function() {
    // do validation here
    var requestedfund = $('#calc-fund-field').val();
    if(requestedfund <= 0){
        $.toaster({
                message : '<span class="">No pending funds to be released!</span>',
                title : 'Message',
                priority : 'danger',
                settings: { timeout : 6000 },
            });
        $('#addForm')[0].reset();
        return false;
    }
    });


    // var demandsTable = $('#demands-table').DataTable({
    //     "processing": true,
    //     "serverSide": true,
    //     "searching": true,
    //     "ajax":
    //     {
    //     "url": APP_URL+"/landholding/funddeclarations",
    //     "type": "GET",
    //     "dataType":"json",
    //     },
    //     columns: [
    //         { data: 'demand_date', name: 'demand_date' },
    //         { data: 'fund_yearmarked', name: 'fund_yearmarked' },
    //         { data: 'amount_in_hand', name: 'amount_in_hand' },
    //         { data: 'fund_requested', name: 'fund_requested' },
    //     ],
    // });

});

function isNumber(event) {
  var inputValue = event.which;
  // allow numbers and null (0), backspace (8), del(127) .
  if(!(inputValue > 47 && inputValue < 58) && (inputValue != 0 && inputValue != 8 && inputValue != 127)) {
      event.preventDefault();
  }
}

function calculateFund() {
    var fund_pending = $('#total_fund_pending').text();
    var fund_request = parseInt(fund_pending) - parseInt($('#fundtobereleased').val());
    (fund_request >= 0) ? $('#calc-fund-field').val(fund_request) : $('#calc-fund-field').val('0')
}
</script>
@endpush
@endif
@endsection
