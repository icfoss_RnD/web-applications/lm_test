@extends('layouts.app')
@section('title', 'Beneficiaries')

@section('breadcrumbs')
<li >/ <a href="#">Stage Four</a> </li>
@endsection

@push('headstyles')
<style>
.error{
    border:red 1px solid;
}
</style>
@endpush

@section('content')
<div>
@if($signature_status == 0 or $signature_status == '')
<div>
    <p class="unsigned-alert">You have not completed beneficiary verification and declaration process.</p>
</div>
@else
<h2 class="meera">മേല്‍ക്കൂര നിര്‍മ്മാ​ണ ഘട്ടം</h2>
<div class="col-md-12">
<table id="example" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
    <thead>
        <tr>
        <th>Beneficiary Id</th>
            <th>Name of Beneficiary</th>
            <th>Address</th>
            <th>Ration Card Number</th>
            <th>Aadhaar Number</th>
            <th>Gender</th>
            <th>Category</th>
        </tr>
    </thead>
</table>
</div>
</div>
<div id="addFormModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <h3 class="meera">നാലാം ഗ‍ഡു ധനസഹായ വിവരങ്ങള്‍</h3>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container" style="width:100%">
        <div class="row">
        <form id="addForm" class="form form-horizontal" method="post">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" />
            <div class="form-group">
                <label class="col-sm-12 meera" for="name">ഗുണഭോക്താവിന്റെ പേര്:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name"   disabled/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 meera" for="addr">ഗുണഭോക്താവിന്റെ മേല്‍ വിലാസം:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="addr" name="addr"  disabled />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 meera" for="ration">റേഷന്‍ കാര്‍ഡ് നമ്പര്‍:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="ration" name="ration" disabled/>
                </div>
            </div>
             <div class="form-group">
             <label class="col-sm-12 meera" for="instaone" disabled>നാലാം ഗഡു തുക: <span id="instaonevalue"></span> </label>
		<input id="instaone" name="instaone" type="hidden" value=""/> 
                <!-- <div class="col-sm-12">
                <input type="radio" class="col-sm-1" id="instaone4" name="instaone" value="40000" checked="checked" />40,000/-
                <input type="radio" class="col-sm-1" id="instaone9" name="instaone" value="90000"/>90,000/-
                </div> -->
            </div>
            <div class="form-group">
                <label class="col-sm-12 meera" for="released">നാലാം ഗഡു നല്‍കിയോ</label>
                <div class="col-sm-12">
                <input type="radio" class="col-sm-1" id="released1" name="released" value="1" />നല്‍കി
                <input type="radio" class="col-sm-1" id="released0" name="released" value="0" checked="checked" />നല്‍കിയിട്ടില്ല
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 meera" for="instadate">നാലാം ഗഡു നല്‍കിയ തീയതി:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control dt-picker" id="instadate" name="instadate" />
                 </div>
                </div>
                <div class="form-group">
                <label class="col-sm-12 meera" for="completed">നാലാം ഘട്ട നിര്‍മ്മാണം പൂര്‍ത്തിയായോ?</label>
                <div class="col-sm-12">
                <input type="radio" class="col-sm-1 meera" id="completed1" name="completed" value="1" />പൂര്‍ത്തിയായി
                <input type="radio" class="col-sm-1 meera" id="completed0" name="completed" value="0"  checked="checked"/>പൂര്‍ത്തിയായിട്ടില്ല
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 meera" for="workingdays">ലഭ്യമാക്കിയ MGNREGS തൊഴിൽ ദിനങ്ങൾ</label>
                <div class="col-sm-12">
                    <input type="text" onkeypress="isNumber(event)" class="form-control" id="workingdays" name="workingdays" />
                 </div>
                </div>
        </form>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>
@push('bodyscripts')
<script>
$(document).ready(function(){
    $('#instadate').datetimepicker({
        lang:'en',
        timepicker:false,
        minDate:'2017/01/01',
        maxDate: '0',
        format:'Y-m-d',
        allowBlank: true,
        scrollInput: false,
    });

    var table = $('#example').DataTable({
        "processing": true,
        "serverSide": true,
        "searching": true,
        "ajax":
        {
        "url": APP_URL+"/landholding/stagefour",
        "type": "GET",
        "dataType":"json"
        },
        columns: [
            {data: 'beneficiary_id', name: 'beneficiary_id'},
            {data: 'beneficiary_name', name: 'beneficiary_name'},
            {data: 'addr_house_num', name: 'addr_house_num'},
            {data: 'ration_number', name: 'ration_number'},
            {data: 'aadhaar', name: 'aadhaar'},
            {data: 'gender', name: 'gender'},
            {data: 'category_id', name: 'category_id'},
            {data: 'installment_release_status', name: 'installment_release_status'},
            {data: 'installment_date', name: 'installment_date'},
            {data: 'amount_released', name: 'amount_released'},
            {data: 'construction_status', name: 'construction_status'},
            {data: 'working_days', name: 'working_days'},


        ],
        columnDefs: [
        {
            render: function(data, type ,row){
              return data + '/\n' +row.addr_house_name + ',\n' +row.addr_house_location +',\n'+row. pincode
            },
            targets: 2
        },
        {
            render: function(data, type ,row){
                var category_name = 'Unknown';
                switch(data){
                    case '1': category_name = 'GENERAL';break;
                    case '2': category_name = 'ST';break;
                    case '3': category_name = 'SC';break;
                    case '4': category_name = 'OBC';break;
                    default: category_name = 'Unknown';
                }
                return category_name;
            },
            targets: 6
        },
        {
            targets: 7,
            visible: false
        },
        {
            targets: 8,
            visible: false
        },
        {
            targets: 9,
            visible: false
        },
        {
            targets: 10,
            visible: false
        },
        {
            targets: 11,
            visible: false
        }
        ],
        "fnRowCallback" : function(nRow, aData, iDisplayIndex)
        {
          // colouring data_rows with installment_release_status=1
          if(aData.installment_release_status == 1){
            $(nRow).addClass('yellow');
          }
          // colouring data_rows with construction_status for st_remote=1
          if(aData.construction_status == 1 && aData.st_remote == 0){
            $(nRow).removeClass('yellow');
            $(nRow).addClass('green');
          }
          return nRow;
        }
    });

    $('#save').on('click',function(){
        var id = $('#id').val();
        var form_data = $('#addForm').serialize();
        var instadate = $("#instadate").val();
        if(instadate == null){
            $('#instadate').addClass('error');
            return;
        }
        $('#instadate').removeClass('error');
	$("#save").prop('disabled', true);
        $('#save').html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Save');
        $.ajax({
        url: APP_URL+"/landholding/stagefour",
        type: "POST",
        dataType: "json",
        data: form_data,
        success: function(result){
            $.toaster({
                message : result.message,
                title : 'Message',
                priority : result.status,
                settings: { timeout : 6000 },
            });
	    $("#save").prop('disabled', false);
            $('#save').html('Save');
            $('#addForm')[0].reset();
            $('#addFormModal').modal('toggle');
            table.ajax.reload();
            }
        });
    });

    $('#example tbody').on( 'click', 'tr', function () {
        var row_data = table.row(this).data();
        $('#id').val(row_data.beneficiary_id);
        $('#name').val(row_data.beneficiary_name);
        $('#addr').val(row_data.addr_house_num + '/' +row_data.addr_house_name + ',' +row_data.addr_house_location +','+row_data. pincode);
        $('#ration').val(row_data.ration_number);
        $('#adhaar').val(row_data.aadhaar);
        $('#gender').val(row_data.gender);
        $('#agreementdate').val(row_data.agreement_date);
        $('#instadate').val(row_data.installment_date);
        $('#workingdays').val(row_data.working_days); //working days
        if (row_data.installment_release_status == 1){
            $('input#released1').prop("checked", true);
            $('input#released0').prop("checked", false);
        }
        else{
            $('input#released1').prop("checked", false);
            $('input#released0').prop("checked", true);
        }
        if (row_data.construction_status == 1){
            $('input#completed1').prop("checked", true);
            $('input#completed0').prop("checked", false);
        }
        else{
            $('input#completed1').prop("checked", false);
            $('input#completed0').prop("checked", true);
        }
        if(row_data.st_remote==1){
          $('#instaonevalue').text("1,20,000.");	  
	}
        else{
          $('#instaonevalue').text("40,000.");
	}

        // On page load, if 'not released' radio button is active  disable installment date
        //alert($('input[id="released1"]').prop("checked"));
        if ($('input[id="released0"]').prop("checked")) {
            $('input#instadate').val('');
            $('input#instadate').prop('disabled',true);
        }
        if ($('input[id="released1"]').prop("checked")) {
            $('input#instadate').prop('disabled',false);
            $("input#instadate").attr("required", true);
        }
        // disable installment date field if 'not released' is clicked
        $('input[id="released0"]').on("click", function(e) {
            $('input#instadate').val('');
            $('input#instadate').prop('disabled','true');
        });
        // enable installment date field if 'released' is clicked
        $('input[id="released1"]').on("click", function(e) {
            $('input#instadate').prop('disabled',false);
            $("input#instadate").attr("required", true);
        });
        $('#addFormModal').modal('toggle');
    });


});
function isNumber(event) {
  var inputValue = event.which;
  // allow numbers and null (0), backspace (8), del(127) .
  if(!(inputValue > 47 && inputValue < 58) && (inputValue != 0 && inputValue != 8 && inputValue != 127)) {
      event.preventDefault();
  }
}
</script>
@endpush
@endif
@endsection
