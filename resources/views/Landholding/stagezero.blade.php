@extends('layouts.app')
@section('title', 'Beneficiaries')

@section('breadcrumbs')
<li >/ <a href="#">Stage Zero</a> </li>
@endsection

@section('content')
@if($signature_status == 0 or $signature_status == '')
<div>
    <p class="unsigned-alert">You have not completed beneficiary verification and declaration process.</p>
</div>
@else
<div class="col-md-12">
<h2 class="meera">ഒന്നാംഘട്ട പരിശോധന പൂർത്തിയാക്കിയ ഗുണഭോക്താക്കൾ</h2>
<table id="example" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
        <thead>
            <tr>
                <th>Beneficiary Id</th>
                <th>Name of Beneficiary</th>
                <th>Address</th>
                <th>Ration Card Number</th>
                <th>Aadhaar Number</th>
                <th>Gender</th>
                <th>Category</th>
            </tr>
        </thead>
    </table>
</div>
</div>
<div id="addFormModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <h3 class="meera">നിർബന്ധിത രേഖകളുടെ പരിശോധന</h3>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container" style="width:100%">
        <div class="row">
        <form id="addForm" class="form form-horizontal" method="post">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" />
            <div class="form-group">
                <label class="col-sm-12 meera" for="name">ഗുണഭോക്താവിന്റെ പേര്:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name"   disabled/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 meera" for="addr">ഗുണഭോക്താവിന്റെ മേല്‍ വിലാസം:</label>
                <div class="col-sm-12">
                    <input type="addr" class="form-control" id="addr" name="addr"  disabled />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 meera" for="ration">റേഷന്‍ കാര്‍ഡ് നമ്പര്‍:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="ration" name="ration" disabled/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12" style="display: inline;padding-right: 0;">
                    <input type="checkbox" class="" id="possession" name="possession" />
                </div>
                <label style="display: inline;margin: 0;padding: 3px;" class="col-sm-12 meera" for="possession">ഭൂമിയുടെ കെെവശാവകാശരേഖ / ആധാരം / പട്ടയം / കൈവശ സാക്ഷ്യപത്രം
                  നേരിട്ടു പരിശോധിച്ചു ബോധ്യപ്പെട്ടു</label>
            </div>
            <div class="form-group">
                <label class="col-sm-12 meera" for="possessioncertificate">ഭൂരേഖയായി സമർപ്പിച്ചിട്ടുള്ളത് :</label>
                <div class="col-sm-12">
                  <select id="possessioncertificate" name="possessioncertificate" class="form-control">
                    <option value="possession_record">കൈവശാവകാശ രേഖ</option>
                    <option value="possession_record_adhaaram">ആധാരം</option>
                    <option value="possession_record_pattayam">പട്ടയം</option>
                    <option value="forestofficer_certificate">ഫോറസ്റ്റ് റേഞ്ച് ഓഫീസർ നൽകിയ സാക്ഷ്യപത്രം</option>
                    <option value="villageofficer_certificate">പഞ്ചായത്തു സെക്രട്ടറിയോ വില്ലേജ് ഓഫീസറോ നൽകിയ സാക്ഷ്യപത്രം</option>
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 meera" for="building_permit_date">ബിൽഡിങ് പെർമിറ്റു നൽകിയ തീയതി:</label>
                <div class="col-sm-12">
                  <input type="text" autocomplete="on" class="form-control dt-picker" id="building_permit_date" name="building_permit_date" required/>
                </div>
              </div>
            <h5>ബാങ്ക് അക്കൗണ്ട് വിവരങ്ങൾ</h5>
            <div class="form-group">
                <label class="col-sm-12 meera" for="account_number">അക്കൗണ്ട് നമ്പർ:</label>
                <div class="col-sm-12">
                    <input type="text" onkeypress="isNumber(event)" class="form-control" id="account_number" name="account_number"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12 meera" for="bank_name">ബാങ്കിന്റെ പേര്:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="bank_name" name="bank_name"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12 meera" for="branch_name">ശാഖയുടെ പേര്:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="branch_name" name="branch_name"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12 meera" for="ifsc_code">IFSC കോഡ്:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="ifsc_code" name="ifsc_code"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12 meera" for="account_holder">അക്കൗണ്ട് ഹോൾഡറുടെ പേര്:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="account_holder" name="account_holder"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12 meera" for="beneficiary_relationship">അക്കൗണ്ട് ഹോൾഡർക്കു ഗുണഭോക്താവുമായുള്ള ബന്ധം:</label>
                <div class="col-sm-12">
                    <select id="beneficiary_relationship" name="beneficiary_relationship" class="form-control" onchange="customRelation()">
                    <option value="self">ഗുണഭോക്താവു തന്നെ</option>
                    <option value="father">അച്ഛന്‍</option>
                    <option value="mother">അമ്മ</option>
                    <option value="husband">ഭര്‍ത്താവ്</option>
                    <option value="wife">ഭാര്യ</option>
                    <option value="son">മകന്‍</option>
                    <option value="daughter">മകൾ</option>
                    <option value="brother">സഹോദരന്‍</option>
                    <option value="sister">സഹോദരി</option>
                    <option value="other" id="other_relation">മറ്റുള്ളവ</option>
                  </select>
                </div>
            </div>

            <div class="form-group" id="custom_relation_wrapper" style="display: none;">
                <label class="col-sm-12 meera" for="custom_relation">മറ്റുള്ളവയെങ്കിൽ
                    അക്കൗണ്ട് ഹോൾഡര്‍ക്കു ഗുണഭോക്താവുമായുള്ള ബന്ധത്തിന്റെ വിശദീകരണം:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="custom_relation" name="custom_relation"/>
                </div>
            </div>

        </form>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@push('bodyscripts')
<script>
$(document).ready(function(){
    customRelation();
    //$("#custom_relation_wrapper").hide();
    $('#building_permit_date').datetimepicker({
        lang:'en',
        timepicker:false,        
        maxDate: '0',
        format:'Y-m-d',
        allowBlank: true,
        scrollInput: false
    });
    var table = $('#example').DataTable({
        "processing": true,
        "serverSide": true,
        "searching": true,
        "ajax":
        {
        "url": APP_URL+"/landholding/stagezero",
        "type": "GET",
        "dataType":"json"
        },
        columns: [
            {data: 'beneficiary_id', name: 'beneficiary_id'},
            {data: 'beneficiary_name', name: 'beneficiary_name'},
            {data: 'addr_house_num', name: 'addr_house_num'},
            {data: 'ration_number', name: 'ration_number'},
            {data: 'aadhaar', name: 'aadhaar'},
            {data: 'gender', name: 'gender'},
            {data: 'category_name', name: 'category_name'},
            {data: 'possession_verified', name: 'possession_verified'},
            {data: 'possession_certificate', name: 'possession_certificate'},
            {data: 'building_permit', name: 'building_permit'},

            {data: 'account_no', name: 'account_no'},
            {data: 'bank_name', name: 'bank_name'},
            {data: 'branch_name', name: 'branch_name'},
            {data: 'ifsc_code', name: 'ifsc_code'},
            {data: 'accountholder_name', name: 'accountholder_name'},
            {data: 'relation_with_beneficiary', name: 'relation_with_beneficiary'},
           

        ],
        columnDefs: [
            {
            render: function(data, type ,row){
                return data + '/\n' +row.addr_house_name + ',\n' +row.addr_house_location +',\n'+row. pincode
            },
            targets: 2
        },
        {
            targets: 7,
            visible: false
        },
        {
            targets: 8,
            visible: false
        },
        {
            targets: 9,
            visible: false
        },
        {
            targets: 10,
            visible: false
        },
        {
            targets: 11,
            visible: false
        },
        {
            targets: 12,
            visible: false
        },
        {
            targets: 13,
            visible: false
        },
        {
            targets: 14,
            visible: false
        },
        {
            targets: 15,
            visible: false
        },
        ]
    });

    $('#save').on('click',function(){
        var id = $('#id').val();
        var form_data = $('#addForm').serialize();
	$("#save").prop('disabled', true);
        $('#save').html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Save');
        $.ajax({
            url: APP_URL+"/landholding/stagezero/"+id,
            type: "PUT",
            dataType: "json",
            data: form_data,
            success: function(result){
                $.toaster({
                    message : result.message,
                    title : 'Message',
                    priority : result.status,
                    settings: { timeout : 6000 },
                });
		$("#save").prop('disabled', false);
        	$('#save').html('Save');
                $('#addForm')[0].reset();
                $('#addFormModal').modal('toggle');
                table.ajax.reload();
            }
        });
    });

    $('#example tbody').on( 'click', 'tr', function () {
        var row_data = table.row(this).data();
        $('#id').val(row_data.beneficiary_id);
        $('#name').val(row_data.beneficiary_name);
        $('#addr').val(row_data.addr_house_num + '/' +row_data.addr_house_name + ',' +row_data.addr_house_location +','+row_data. pincode);
        $('#ration').val(row_data.ration_number);
        $('#adhaar').val(row_data.aadhaar);
        $('#possessioncertificate').val(row_data.possession_certificate);
        $('#gender').val(row_data.gender);
        $('#building_permit_date').val(row_data.building_permit);
        $('#account_number').val(row_data.account_no);
        $('#bank_name').val(row_data.bank_name);
        $('#branch_name').val(row_data.branch_name);
        $('#ifsc_code').val(row_data.ifsc_code);
        $('#account_holder').val(row_data.accountholder_name);

        var relations = ['self', 'father', 'mother', 'husband', 'wife', 'son', 'daughter', 'brother', 'sister'];
        if (jQuery.inArray(row_data.relation_with_beneficiary, relations) == -1 && (row_data.relation_with_beneficiary) !== null ){  
            $("#beneficiary_relationship").html('<option value="'+row_data.relation_with_beneficiary+'" selected>'+row_data.relation_with_beneficiary+'</option>'+"<option value='self'>ഗുണഭോക്താവു തന്നെ</option><option value='father'>അച്ഛന്‍</option><option value='mother'>അമ്മ</option><option value='husband'>ഭര്‍ത്താവ്</option><option value='wife'>ഭാര്യ</option><option value='son'>മകന്‍</option><option value='daughter'>മകൾ</option><option value='brother'>സഹോദരന്‍</option><option value='sister'>സഹോദരി</option><option value='other'>മറ്റുള്ളവ</option>");
        }
        $('#beneficiary_relationship').val(row_data.relation_with_beneficiary);  
        //$('#possession').val(row_data.possession_verified);
        if(row_data.possession_verified==1)
            $('#possession').prop("checked", true);
        else
            $("#possession").prop("checked", false); 

        $('#addFormModal').modal('toggle');
        customRelation()
    });
});

// number validation
function isNumber(event) {
  var inputValue = event.which;
  // allow numbers and null (0), backspace (8), del(127) .
  if(!(inputValue > 47 && inputValue < 58) && (inputValue != 0 && inputValue != 8 && inputValue != 127)) {
      event.preventDefault();
  }
}

// to activate custom input field if 'other' is selected from
// beneficiary relationship select box
function customRelation() {
        if ($("#beneficiary_relationship option:selected").val() == 'other') {
            $("#custom_relation_wrapper").show();
        } else {
            $("#custom_relation_wrapper").hide();
        }
    }
</script>
@endpush
@endif
@endsection
