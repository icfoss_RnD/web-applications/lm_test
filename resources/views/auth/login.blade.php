@extends('layouts.master')

@section('content')
<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div> -->
                <div class="grid">
                    <form class="form login" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="login__body">

                                <div class="{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                                    <div class="form__field">
                                        <input id="name" type="text" class="" name="name" value="{{ old('name') }}" placeholder="Username" required autofocus />
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="{{ $errors->has('password') ? ' has-error' : '' }} col-md-12">

                                    <div class="form__field">
                                        <input id="password" type="password" class="" name="password" placeholder="Password" required />
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <!-- <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="form__field">
                                    <div class="col-md-12">
                                        <button type="submit" class="loginbutton pull-right">
                                            Login
                                        </button>
                                    </div>
                                </div>
                                <div class="form__field">
                                    <div class="col-md-12">
                                        <button type="button" style="margin-left: 0px" onclick="location.href='{{route('ext_login')}}'" class="loginbutton btn-block">
                                            LSG Users click here for Login
                                        </button>
                                    </div>
                                </div>
                        </div>
                    </form>
                </div>
            <!-- </div>
        </div>
    </div>
</div> -->
@endsection
