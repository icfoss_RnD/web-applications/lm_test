@extends('layouts.error')
@section('content')
    <div class="row" style="margin-top:100px">
    <div class="col-md-4 col-md-offset-4 text-center">
    <h2>404<br/><br/>Page not Found.</h2><br/>
    <a class="btn btn-primary btn-block" href="javascript:void(0)" onclick="window.history.go(-1); return false;">Click here to go back</a>
    </div>
    </div>
@endsection