@extends('layouts.error')
@section('content')
    <div class="row" style="margin-top:100px">
    <div class="col-md-4 col-md-offset-4 text-center">
    <h2>500<br/><br/>Internal Server Error.</h2><br/>
    <a class="btn btn-primary btn-block" href="#" onclick="window.history.go(-1);">Click here to go back</a>
    </div>
    </div>
@endsection