<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

  <head>
  <meta charset="utf-8" />
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <meta name="author" content="ICFOSS">
  <meta name="description" content="LIFE Mission Project">
  <meta name="keywords" content="ICFOSS">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport" />
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('/img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{URL::asset('/img/life.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>LIFE Mission - @yield('title')</title>
  <!-- style sheets -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css"/>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  @stack('headstyles')
  <!-- CSS Files -->
  <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

    <!-- Custom CSS -->
    <link href="{{URL::asset('/css/stylish-portfolio.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/css/introstyle.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/css/demo.css')}}" rel="stylesheet">




  <link href="{{URL::asset('/css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet" />
  <link href="{{URL::asset('/css/app.css')}}" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
  <!-- <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" /> -->
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <!-- scripts -->
<script>
      var APP_URL = '{{URL::to("/")}}';
  </script>
<style>
h2{
    border-bottom:0px;
}
</style>
  </head>

<body>
	<div class="center_align">
		<img src="{{asset('/img/intro-logo.png')}}">
	</div>
	<div style="position:relative;background: url({{asset('/img/2.jpg')}}) no-repeat;background-size: cover;height:51em;">
		<div style="position:absolute;" class="paragraph_padding">
			<h2 id="small_black_heading" ><b>ഘട്ടം 1 ( 2017- 2018)</b></h2>
			<h1 class=" meera blue_heading" >പൂർത്തീകരിക്കാത്ത</h1>
			<h1 class=" meera blue_heading" >വീടുകളുടെ</h1>
			<h1 class="meera blue_heading" >പൂർത്തീകരണം</h1>
			<div style="text-align:right;width:90%;padding-top:17%;">
				<br>
				<h3 class="dyuthi blue_heading">അപൂർണ ഭവനങ്ങളും</h3>
				<h3 class="dyuthi blue_heading" style="line-height:0%;">പൂർത്തിയാക്കിക്കഴി‍ഞ്ഞു</h3>
			</div><br>
			<p class="meera" id="paragraph_font_size_1">വിവിധ സര്‍ക്കാര്‍ പദ്ധതികളില്‍ മുന്‍കാലത്ത് ഭാഗിക ധനസഹായം കെെപ്പറ്റുകയും എന്നാല്‍ വിവിധ കാരണങ്ങളാല്‍ വീടുപണി പൂർത്തീകരിക്കാനാകാതെ പോവുകയും ചെയ്ത അപൂര്‍ണ്ണ ഭവനങ്ങള്‍ക്ക് അധിക ധനസഹായവും സാങ്കേതിക പിന്തുണയും നല്‍കി പൂർത്തീകരിക്കുക. </p>
            <br>
            <span class="link_img_left"><a href="{{asset('/img/phase1.pdf')}}" target="_blank"><button class="btn btn-primary">പ്രവർത്തന മാർഗരേഖ.</button></a></span>
            <span class="link_img_left" style="padding-left:14%"><a href="http://www.lifephase1.org/(S(zzh5xcfcis4srleq2antmqh2))/MIS.aspx" target="_blank"><button class="btn btn-primary">നേട്ടം ഇതുവരെ</button></a></span>
            <span class="link_img_left" style="padding-left:14%"><a href="http://lifephase1.org/(S(5yku0ry4j0wu05qzcc2qo3l0))/login" target="_blank"><button class="btn btn-primary">ഘട്ടം 1 ലോഗിൻ</button></a></span>
        </div>
	</div>
	<div style="position:relative;background: url({{asset('/img/3.jpg')}}) no-repeat;background-size: cover;height:51em;">
		<div style="position:absolute;top:10%;padding-right: 13%;" class="paragraph_padding right_align">
			<h2 id="small_black_heading" ><b>ഘട്ടം 2 ( 2018- 2019)</b></h2>
			<h1 class=" meera blue_heading" >സ്വന്തമായി ഭൂമിയുള്ള  </h1>
			<h1 class=" meera blue_heading" >ഭവനരഹിതർക്ക്  ഭവന</h1>
			<h1 class="meera blue_heading" >ധനസഹായം</h1>
			<br>
            <p class="left_align">
                <ul id="paragraph_font_size_2">
                <li class="meera"> സ്വന്തമായി ഭൂമിയുണ്ടെങ്കിലും വീടെന്ന സ്വപ്നം സാക്ഷാത്കരിക്കാൻ സമൂഹത്തിന്റെ കൈത്താങ്ങ് ആവശ്യമായ ദുർബല വിഭാഗങ്ങൾക്ക്</li>
                <li class="meera"> വിദൂരസ്ഥിത സങ്കേതങ്ങളിലെ പട്ടികവർഗ വിഭാഗങ്ങൾക്ക് 6 ലക്ഷം രൂപയും മറ്റെല്ലാ വിഭാഗങ്ങൾക്കും 4 ലക്ഷം രൂപയും ധനസഹായം.</li>
                </ul>
            </p>
			<span class="link_img_right"><a href="{{asset('/img/phase2.pdf')}}" target="_blank"><button class="btn btn-primary">പ്രവർത്തന മാർഗരേഖ</button></a></span>
            <span class="link_img_left" style="padding-left:14%"><a href="{{url('/ext_login')}}" target="_blank"><button class="btn btn-primary">ഘട്ടം 2 ലോഗിൻ</button></a></span>
		</div>
    </div>
	<div style="position:relative;background: url({{asset('/img/4.jpg')}}) no-repeat;background-size: cover;height:51em;">
		<div style="position:absolute;" class="paragraph_padding">
			<h2 id="small_black_heading" ><b>ഘട്ടം 3 ( 2018- 2021)</b></h2>
			<h1 class=" meera blue_heading" >ഭൂരഹിതഭവനരഹിതരുടെ</h1>
			<h1 class=" meera blue_heading" >പുനരധിവാസം</h1>
			<br>
			    <p class="meera">
                <ul id="paragraph_font_size_3">
                <li class="meera"> സ്വന്തമായി ഭൂമിയില്ലാത്ത ഭവനരഹിതരെ ഭവനസമുച്ചയങ്ങൾ നിർമ്മിച്ചു പുനരധിവസിപ്പിക്കുക.</li>
                <li class="meera"> കൈമാറ്റാവകാശം  ഇല്ലാത്തവീടുകളാണ് ഭവന സമുച്ചയങ്ങൾ. എന്നാൽ അവയോടൊപ്പം താമസക്കാർക്ക് അവരുടെ കുട്ടികളുടെയും  പ്രായമായവരുടെയും സംരക്ഷണത്തിനും സമൂഹികക്ഷേമപദ്ധതി സഹായങ്ങൾ ലഭിക്കുന്നതിനും സംവിധാനമുണ്ടായിരിക്കും.</li>
                <li class="meera">2018-2019 ലെ ലക്ഷ്യം ഓരോ ജില്ലയിലും ഓരോ പൈലറ്റ് LIFE ഭവന സമുച്ചയങ്ങൾ</li>
                </p>
			<span class="link_img_left" ><a href="{{asset('/img/Pilot Apartment Complex suggested locations.pdf')}}" target="_blank"><button class="btn btn-primary">പൈലറ്റ്‌ LIFE ഭവന സമുച്ചയങ്ങൾ</button></a></span>
	    </div>
	</div>
	<div style="position:relative;background:url({{asset('/img/5.jpg')}}) no-repeat;background-size: cover;height:51em;">
		<div style="position:absolute;width:100%;padding-right: 13%;" class="paragraph_padding right_align">
			<h2 id="small_black_heading" ><b>ഘട്ടം 4 ( 2019- 2021)</b></h2>
			<h1 class=" meera blue_heading" >വാസയോഗ്യമല്ലാത്ത വീടുകളുടെ</h1>
			<h1 class=" meera blue_heading" >നവീകരണം</h1>
			<br>
			<p class="meera" id="paragraph_font_size_4">തീർത്തും നിർധനവിഭാഗങ്ങൾ വസിക്കുന്ന, അപകടകരമായവിധം<br> വാസയോഗ്യമല്ലാതായ വീടുകളുടെ നവീകരണത്തിനുള്ള സഹായം.<br>പ്രവർത്തന പദ്ധതി രൂപീകരിച്ചു വരുന്നു.       </p>
		</div>
	</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{URL::asset('/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
</body>
</html>
