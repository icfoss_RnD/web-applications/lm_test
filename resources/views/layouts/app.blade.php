<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{URL::asset('/img/life.png')}}">
    <title>LIFE Mission - @yield('title')</title>

    <!-- Bootstrap core CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" />
    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css"/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    @stack('headstyles')
    <!-- CSS Files -->
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

    <!-- Custom CSS -->
    <link href="{{URL::asset('/css/stylish-portfolio.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/css/introstyle.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/css/app.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.datetimepicker.min.css')}}" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <!-- <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet"> -->
    <!-- scripts -->
    <script>
      var APP_URL = '{{URL::to("/")}}';
    </script>
    @stack('headscripts')
</head>

<body class="fixed-nav sticky-footer" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" id="mainNav">
    <a class="navbar-brand" target="_blank" href="https://lifemission.lsgkerala.gov.in/en">
      <img style="height:40px;" src="{{asset('/img/life-mission-logo-eng.png')}}" />
    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item">
          <a class="nav-link" href="#">
            <i class="fa fa-fw"></i>
            <span class="nav-link-text"></span>
          </a>
        </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-calendar-check-o"></i>
            <span class="nav-link-text">വിശദാംശ പരിശോധന</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
         <a class="dropdown-item" href="{{url('/landholding/beneficiaries')}}">
             <i class="fa fa-fw fa-calendar-check-o"></i>
             <span class="nav-link-text">ഗുണഭോക്തൃഅംഗീകാര പരിശോധന</span>
         </a>
         <a class="dropdown-item" href="{{url('/landholding/declarations')}}">
             <i class="fa fa-fw fa-pencil-square-o"></i>
             <span class="nav-link-text">സാക്ഷ്യപ്രസ്താവന</span>
         </a>
        @if(isset($signature_status) AND $signature_status == 1)
         <a class="dropdown-item" href="{{url('/landholding/stagezero')}}">
             <i class="fa fa-fw fa-calendar-check-o"></i>
             <span class="nav-link-text">നിർബന്ധിത രേഖകളുടെ പരിശോധന</span>
         </a>
	@endif
        </div>
        </li>

        @if(isset($signature_status) AND $signature_status == 1)

        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-users"></i>
            <span class="nav-link-text">ഫണ്ട് വിതരണം</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{url('/landholding/stageone')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">ഫണ്ട് വിതരണം - ഘട്ടം 1</span>
            </a>
            <a class="dropdown-item" href="{{url('/landholding/stagetwo')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">ഫണ്ട് വിതരണം - ഘട്ടം 2</span>
            </a>
            <a class="dropdown-item" href="{{url('/landholding/stagethree')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">ഫണ്ട് വിതരണം - ഘട്ടം 3</span>
            </a>
            <a class="dropdown-item" href="{{url('/landholding/stagefour')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">ഫണ്ട് വിതരണം - ഘട്ടം 4</span>
            </a>
            <a class="dropdown-item" href="{{url('/landholding/stagefive')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">ഫണ്ട് വിതരണം - ഘട്ടം 5</span>
            </a>
        </div>
        </li>
        <li class="nav-item ">
           <a class="nav-link" href="{{url('/landholding/funddeclarations')}}">
               <i class="fa fa-fw fa-rupee"></i>
              <span class="nav-link-text">ധനവിനിയോഗ വിശദാംശം</span>
           </a>
        </li>

        
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-fw fa-users"></i>
              <span class="nav-link-text">Local Body Migration</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{url('/landholding/initiatetransferrequest')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">Initiate  Request</span>
            </a>
            <a class="dropdown-item" href="{{url('/landholding/accepttransferrequest')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">Accept Requests </span>
            </a>
            </div>
            </li>

         <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-fw fa-users"></i>
              <span class="nav-link-text">അധിക പട്ടികയും മാറ്റങ്ങളും</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{url('/landholding/category/1/applicants')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">ത.സ്വ.ഭ സ്ഥാപനത്തിന്റെ അംഗീകാര ലിസ്റ്റിൽ ഉൾപ്പെടാത്തവ</span>
            </a>
            <a class="dropdown-item" href="{{url('/landholding/category/2/applicants')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">ഭൂമിയുള്ള ഭവനരഹിതരുടെ പട്ടികയിലേക്കുള്ള കാറ്റഗറി മാറ്റം </span>
            </a>
            <a class="dropdown-item" href="{{url('/landholding/category/3/applicants')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">ഗ്രാമസഭകൾ നിർദ്ദേശിച്ച സപ്ലിമെന്ററി ഗുണഭോക്തൃപ്പട്ടിക</span>
            </a>
            <a class="dropdown-item" href="{{url('/landholding/category/4/applicants')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">2018ലെ മഴക്കെടുതിയിൽ വീട് നഷ്ടമായ LIFE ഗുണഭോക്താക്കൾ</span>
            </a>
            <!-- <a class="dropdown-item" href="{{url('/landholding/category/5/applicants')}}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
               <span class="nav-link-text">ഗുണഭോക്തൃ പട്ടികയിൽ പേരുണ്ടെങ്കിലും അർഹത തെളിയിച്ചിട്ടില്ലാത്തവർ</span>
            </a> -->
             </div>
	       </li>
        @endif
      </ul>
      <ul class="navbar-nav ml-auto dropdown">
          <li class="nav-item">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user"></i> Hi {{ session('lb_name') }}
            </a>
             <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
             <i class="fa fa-sign-out fa-fw"></i>Logout
           </a>
       </div>
          </li>
          <li class="nav-item">

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
    	  </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <li><a href="#">Home</a></li>
            <li>/ <a href="#">Phase 2 </a></li>
            @yield('breadcrumbs')
        </li>
      </ol>
      <br/>


      <div class="row">
        <div class="col-12">
            @yield('content')
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © LIFE Mission 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    
    <!-- Bootstrap core JavaScript-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{URL::asset('/js/core/popper.min.js')}}" type="text/javascript"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://scottoffen.github.io/jquery.toaster/jquery.toaster.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>
    <!-- <script src="{{URL::asset('/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script> -->
    <script src="{{URL::asset('/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <!-- <script src="{{URL::asset('/js/material-dashboard.min.js?v=2.1.0')}}" type="text/javascript"></script> -->
    <script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script>
    url=window.location;
    // for sidebar menu entirely but not cover treeview
    $('#exampleAccordion a').filter(function() {
      return this.href == url;
    }).parent().addClass('active');
    </script>
    @stack('bodyscripts')
</body>
</html>
