<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="author" content="ICFOSS">
        <meta name="description" content="LIFE Mission Project">
        <meta name="keywords" content="ICFOSS">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LIFE Mission - @yield('title')</title>
        <!-- style sheets -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap.min.css" />
        @stack('headstyles')
        <!-- scripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script>
            var APP_URL = '{{URL::to("/")}}';
        </script>
        @stack('headscripts')
    </head>
    <body class="align">
    <div class="container">
        @yield('content')
    </div>
    @stack('bodyscripts')
    </body>
</html>
