@extends('layouts.app')
@section('title', '| Permissions')
@section('content')
<div class="col-md-10 col-md-offset-1">
<h1><i class="fa fa-key"></i>Permissions Management
<a href="{{ route('users.index') }}" class="btn btn-default pull-right">Users</a>
<a href="{{ route('roles.index') }}" class="btn btn-default pull-right" style="margin-right:10px">Roles</a>
<a href="{{ URL::to('permissions/create') }}" class="btn btn-success">Add Permission</a>
</h1>
<hr>
<div class="table-responsive">
<table class="table table-bordered table-striped">
<thead>
<tr>
<th class="col-sm-6">Permissions</th>
<th class="col-sm-1">Action</th>
</tr>
</thead>
<tbody>
@foreach ($permissions as $permission)
<tr>
<td class="col-sm-6">{{ $permission->name }}</td>
<td class="col-sm-1">
    <a href="{{ URL::to('permissions/'.$permission->id.'/edit') }}" class="btn btn-warning btn-block pull-left" style="margin-bottom:10px">Edit</a>
    {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}
    {!! Form::close() !!}
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>
@endsection
