@extends('layouts.app')
@section('title', '| Roles')
@section('content')
<div class="col-lg-10 col-lg-offset-1">
    <h1><i class="fa fa-key"></i> Roles Management
    <a href="{{ route('users.index') }}" class="btn btn-default pull-right">Users</a>
    <a href="{{ route('permissions.index') }}" class="btn btn-default pull-right" style="margin-right:10px">Permissions</a>
    <a href="{{ URL::to('roles/create') }}" class="btn btn-success">Add Role</a>
    </h1>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="col-sm-4">Role</th>
                    <th class="col-sm-6">Permissions</th>
                    <th class="col-sm-1">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $role)
                <tr>
                    <td class="col-sm-4">{{ $role->name }}</td>
                    <td class="col-sm-6" style="word-break: break-all;">
                        <ul>
                            @foreach ($role->permissions()->pluck('name') as $permission)
                            <li>
                                {{ $permission }}
                            </li>
                            @endforeach
                        </ul>
                    </td>
                    <td class="col-sm-1">
                    <a href="{{ URL::to('roles/'.$role->id.'/edit') }}" class="btn btn-warning btn-block pull-left" style="margin-bottom:10px">Edit</a>
                    {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}
                    {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
