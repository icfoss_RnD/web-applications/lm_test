<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('/introduction');
});





Route::group(['middleware' => ['role:Local Body,Access_backend'], 'prefix' => 'landholding', 'namespace' => 'Landholding'], function () {
    //admin routes
    Route::resource('/beneficiaries','BeneficiaryController');
    Route::resource('/declarations','DeclarationController');
    Route::resource('/stagezero','StageZeroController');
    Route::resource('/stageone','StageOneController');
    Route::resource('/stagetwo','StageTwoController');
    Route::resource('/stagethree','StageThreeController');
    Route::resource('/stagefour','StageFourController');
    Route::resource('/stagefive','StageFiveController');
    Route::resource('/funddeclarations','FundDeclarationController');
    Route::resource('/initiatetransferrequest','LocalbodyMigrationController');
    Route::resource('/accepttransferrequest','AcceptTransferRequestController');


    // new applicants
    // Route::resource('/newapplicants','AddApplicantsController');
    Route::group(['prefix' => 'category/{index}'], function($index){
        Route::resource('/applicants','AddApplicantsController');
        Route::post('/submitapplicants','AddApplicantsController@submitapplicants');

    });

    


    //temporary denial for demand page submission
    Route::post('/funddeclarations',function(){
        abort('403');
    });
});
Auth::routes();

Route::get('/home', function () {
    return redirect('landholding/beneficiaries');
});
//Route::resource('users', 'UserController');
//Route::resource('roles', 'RoleController');
//Route::resource('posts', 'PostController');
//Route::resource('permissions','PermissionController');

//routes for IKM CentralAuthService
Route::get('ext_login','Auth\LoginController@ext_login')->name('ext_login');
Route::get('verify_ext_login','Auth\LoginController@verify_ext_login');
